/**
 * @param config
 * @constructor
 */
window.CheckList = function (config) {
    let _self = this;
    this.classChangeInput = config.classChangeInput || 'bg-warning';
    this.saveButton = config.saveButton;
    this.inputNote = config.inputNote;
    this.checkList = config.checkList;
    this.prevLocation = config.prevLocation;
    this.savePath = config.savePath;
    this.alertContainer = config.alertContainer;
    this.alertCount = 0;
    this.csrf = config.csrf;

    $(_self.inputNote).change(function () {
        _self.valChange($(this));
    });

    $(_self.saveButton).click(function () {
        _self.saveList();
    });

    $(_self.checkList).bind('dataChange', function () {
        _self.dataChange();
    });

    $(_self.checkList).bind('dataActual', function () {
        _self.dataActual();
    });
};

CheckList.prototype.valChange = function (elem) {
    let _self = this;
    if (elem.val() === elem.attr('data-original')) {
        elem.removeClass(_self.classChangeInput);
    } else {
        elem.addClass(_self.classChangeInput);
        $(_self.checkList).trigger('dataChange');
    }
};

CheckList.prototype.saveList = function (callback) {
    let _self = this;

    let jqxhr = $.post(
        _self.savePath,
        {
            "csrf": _self.csrf,
            "data": _self.searchChangeData()
        },
        'json'
    ).done(
        function (data) {
            if (!!data['errors'].length) {
                for (let item in data['errors']) {
                    _self.checkListAlert(
                        data['errors'][item],
                        'danger',
                        0
                    );
                }
            } else {
                _self.checkListAlert(
                    'Данные сохранены',
                    'success'
                );
                $(_self.checkList).trigger('dataActual');
                if (typeof callback === 'function') {
                    callback();
                }
            }
        }
    ).fail(function () {
        _self.checkListAlert(
            'Ошибка сохранения данных',
            'danger'
        );
    }).always(function (data) {
        _self.csrf = data.csrf;
    });

    _self.checkListAlert(
        'Идёт сохранение',
        'info'
    );
};

CheckList.prototype.searchChangeData = function () {
    let _self = this;
    let data = [];
    $('.' + _self.classChangeInput).each(function (indx, element) {
        let jqEl = $(element);
        data.push({
            "val": jqEl.val(),
            "student": jqEl.attr('data-student'),
            "discipline": jqEl.attr('data-discipline'),
            "group": jqEl.attr('data-group'),
            "type": jqEl.attr('data-type'),
            "relation": jqEl.attr('data-relation'),
        })
    });

    return data;
};

CheckList.prototype.dataChange = function () {
    let _self = this;
    $(_self.checkList).addClass('list-change');
    $(_self.saveButton).removeAttr('disabled');
};

CheckList.prototype.dataActual = function () {
    let _self = this;
    $(_self.checkList).removeClass('list-change');
    $(_self.saveButton).attr('disabled', 'disabled');
    $(`.${_self.classChangeInput}`).removeClass(_self.classChangeInput);
};

CheckList.prototype.checkListAlert = function (text, type, duration) {
    duration = (duration || duration === 0) ? duration : 3000;
    let _self = this;
    let alertCount = _self.alertCount;
    let tpl = `<div id="alert-check-list-${alertCount}" class="alert alert-${type} alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">×</span></button>${text}</div>`;

    $(_self.alertContainer).append(tpl);
    if (!!duration) {
        setTimeout(function () {
            $(`#alert-check-list-${alertCount}`).remove();
        }, duration);
    }
    _self.alertCount++;
};