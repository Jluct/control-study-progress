window.GroupNote = function (settings) {
    let _self = this;
    _self.userName = settings.userName;
    _self.submitButton = settings.submitButton;
    _self.noteTextArea = settings.noteTextArea;
    _self.pageSize = settings.pageSize;
    _self.data = settings.data;
    _self.alertCount = settings.alertCount;
    _self.savePath = settings.savePath;
    _self.deletePath = settings.deletePath;
    _self.editButtonClass = settings.editButtonClass;
    _self.labelContainer = settings.labelContainer;
    _self.cancelButtonOperation = settings.cancelButtonOperation;
    _self.editLabel = settings.editLabel;
    _self.deleteButtonClass = settings.deleteButtonClass;
    _self.notePanelTpl = settings.notePanelTpl;
    _self.groupNoteContainer = settings.groupNoteContainer;

    _self.submitButton.click(function () {
        _self.blockButton();
        _self.send();
    });

    $('.panel .' + _self.editButtonClass).click(function (e) {
        e.preventDefault();
        _self.editNote($(this));
    });

    _self.labelContainer
        .on('click', "." + _self.cancelButtonOperation, function () {
            _self.cancelEditNote($(this));
        });

    $('.panel')
        .on('click', "." + _self.deleteButtonClass, function () {
            _self.blockButton();
            _self.deleteNote($(this));
        })
};

GroupNote.prototype.deleteNote = function (elem) {
    let _self = this;
    let panel = elem.parents('.panel');

    $.ajax(_self.deletePath + panel.attr('data-id') + '/', {
        "data": {
            "csrf": _self.submitButton.attr('data-csrf')
        },
        "dataType": "Json",
        "type": "DELETE"
    }).done(function (data) {
        _self.addAlert({'classAlert': 'alert-warning', 'text': 'Сообщение удалено'});
    }).always(function (data) {
        _self.activateButton();
        _self.submitButton.attr('data-csrf', data.csrf);
    });

    elem.parents('.study-note').remove();
    _self.labelContainer.html('');
};

GroupNote.prototype.editNote = function (elem) {
    let _self = this;
    _self.labelContainer.html('');
    let panel = elem.parents('.panel');
    panel.find('.' + _self.editButtonClass).hide();
    panel.find('.' + _self.deleteButtonClass).hide();
    _self.noteTextArea.val($.trim(panel.children('.panel-body').text())).focus();
    _self.labelContainer.append(_self.editLabel.html());
    _self.data.id = panel.attr('data-id');
};

GroupNote.prototype.cancelEditNote = function (elem) {
    let _self = this;
    elem.parents('alert').remove();
    _self.noteTextArea.val('');
    $('.' + _self.editButtonClass).show();
    $('.' + _self.deleteButtonClass).show();
    delete _self.data.id;
};

GroupNote.prototype.addNote = function (data) {
    let _self = this;
    let panelTpl = $(_self.notePanelTpl.html());
    panelTpl.find('.panel').attr('data-id', data.id);
    panelTpl.find('.panel-body').text(data.text);
    panelTpl.find('.panel-footer').text(data.date);
    _self.groupNoteContainer.append(panelTpl);
};

GroupNote.prototype.blockButton = function () {
    let _self = this;
    _self.submitButton.attr('disabled', 'disabled');
};

GroupNote.prototype.activateButton = function () {
    let _self = this;
    _self.submitButton.removeAttr('disabled');
};

GroupNote.prototype.send = function () {
    let _self = this;

    _self.data.text = _self.noteTextArea.val();
    $.post(_self.savePath, {
        "group_note": _self.data,
        "csrf": _self.submitButton.attr('data-csrf')
    }, 'Json').done(function (data) {
        data.text = _self.data.text;
        _self.addNote(data);
        _self.labelContainer.html('');
        _self.noteTextArea.val('');
        _self.addAlert({'classAlert': 'alert-success', 'text': 'Сообщение сохранено'});
    }).fail(function (data) {
        _self.addAlert({
            'classAlert': 'alert-danger',
            'text': 'Ошибка сохранения. ' + data.errors
        });
    }).always(function (data) {
        _self.activateButton();
        _self.submitButton.attr('data-csrf', data.csrf);
    });
};

GroupNote.prototype.addAlert = function (alertSettings) {
    let _self = this;

    _self.alertCount++;
    var alert = $('#group-note-alert').html();
    alert = $(alert).addClass(alertSettings.classAlert).attr('id', 'alert-note-' + _self.alertCount);
    alert.children('.data').text(alertSettings.text);
    $('#group-note-alert-container').append(alert);
    setTimeout(function () {
        $('#alert-note-' + _self.alertCount).remove();
    }, 2000);
};