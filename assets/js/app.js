/**
 yarn run encore dev
 yarn run encore dev --watch
 yarn run encore production

 */

var $ = require('jquery');
window.$ = $;
require('bootstrap-sass');
require('../css/app.scss');
require('./check-list');
require('./GroupNote');

$(document).ready(function () {
    $('[data-toggle="popover"]').popover();
});