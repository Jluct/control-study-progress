<?php

namespace ApiBundle\Tests\Controller;


use Symfony\Component\HttpFoundation\Response;

/**
 * Class StudyRelationControllerTest
 * @package ApiBundle\Tests\Controller
 */
class StudyRelationControllerTest extends DefaultControllerTest
{
    /**
     * StudyRelationControllerTest constructor.
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->setRootProperty('study_relation');
    }

    public function testRead()
    {
        $url = '/api/study-relation/read';
        $this->logIn();
        $this->pageFound($url, $this->getRootProperty());
        $this->pageNotFound(sprintf('%s/%s', $url, 999), $this->getRootProperty());
    }

    public function testCreate()
    {
        $url = '/api/study-relation/create/';
        $this->logIn();

        $requestParam = [
            'teacher' => 1,
            'discipline' => 2,
            'studyGroup' => 8882,
            'externalId' => 9991
        ];
        $this->createEntity($url, $this->getRootProperty(), $requestParam);
        $this->createNoValidEntity($url, $this->getRootProperty(), [
            'teacher' => 0,
            'discipline' => 0,
        ]);
        $this->createNoValidEntity($url, $this->getRootProperty(), $requestParam);
    }

    public function testUpdate()
    {
        $url = '/api/study-relation/update/';
        $this->logIn();

        $externalId = 9991;
        $requestParam = [
            'teacher' => 1,
            'discipline' => 2,
            'studyGroup' => 8883
        ];
        $this->updateEntity(sprintf('%s%s', $url, $externalId), $this->getRootProperty(), $requestParam);
        $this->updateEntityWithNoValidData(
            sprintf('%s%s', $url, $externalId),
            $this->getRootProperty(),
            ['teacher' => '00000']
        );
        $this->updateEntityWithNoValidData(
            sprintf('%s%s', $url, $externalId),
            $this->getRootProperty(),
            ['discipline' => '', 'teacher' => '0000']
        );
        $this->updateEntityWithNoValidData(
            sprintf('%s%s', $url, '000'),
            $this->getRootProperty(),
            $requestParam,
            Response::HTTP_NOT_FOUND
        );
    }

    public function testDelete()
    {
        $url = '/api/study-relation/delete/';
        $this->logIn();
        $externalId = 9991;
        $this->deleteEntity(sprintf('%s%s', $url, $externalId), $this->getRootProperty());
        $this->deleteNoIssetEntity(
            sprintf('%s%s', $url, '000'),
            $this->getRootProperty(),
            [],
            Response::HTTP_NOT_FOUND
        );
    }
}