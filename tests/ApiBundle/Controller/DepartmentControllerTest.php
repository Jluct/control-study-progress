<?php

namespace ApiBundle\Tests\Controller;

/**
 * Class DepartmentControllerTest
 * @package ApiBundle\Tests\Controller
 */
class DepartmentControllerTest extends DefaultControllerTest
{
    /**
     * DisciplineControllerTest constructor.
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->setRootProperty('department');
    }

    public function testView()
    {
        $url = '/api/department/read';
        $this->logIn();
        $this->pageFound($url, $this->getRootProperty());
        $this->pageNotFound(sprintf('%s/%s', $url, 999), $this->getRootProperty());
    }
}