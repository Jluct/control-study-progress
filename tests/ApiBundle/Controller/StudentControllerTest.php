<?php

namespace ApiBundle\Tests\Controller;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class StudentControllerTest
 * @package ApiBundle\Tests\Controller
 */
class StudentControllerTest extends DefaultControllerTest
{
    /**
     * DisciplineControllerTest constructor.
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->setRootProperty('student');
    }

    public function testView()
    {
        $url = '/api/student/read';
        $this->logIn();
        $this->pageFound($url, $this->getRootProperty());
        $this->pageNotFound(sprintf('%s/%s', $url, 999), $this->getRootProperty());
    }

    public function testCreate()
    {
        $url = '/api/student/create/';
        $this->logIn();
        $requestParam = [];

        for ($i = 0; $i < 10; $i++) {
            $requestParam = [
                'name' => sprintf('Студент-%s', $i),
                'externalId' => '999' . ($i + 1),
                'studyGroup' => 8881
            ];
            $this->createEntity($url, $this->getRootProperty(), $requestParam);
        }
        $this->createNoValidEntity($url, $this->getRootProperty(), [
            'name' => sprintf('Студент-%s', 9999)
        ]);
        $this->createNoValidEntity($url, $this->getRootProperty(), $requestParam);
    }

    public function testUpdate()
    {
        $url = '/api/student/update/';
        $this->logIn();
        $externalId = '';

        for ($i = 0; $i < 5; $i++) {
            $externalId = '999' . ($i + 1);
            $requestParam = [
                'name' => sprintf('Студент-изменённый-%s', $i)
            ];
            if ($i == 0) {
                $requestParam['externalId'] = 88899;
            }
            $this->updateEntity(sprintf('%s%s', $url, $externalId), $this->getRootProperty(), $requestParam);
        }
        $this->updateEntityWithNoValidData(
            sprintf('%s%s', $url, $externalId),
            $this->getRootProperty(),
            ['name' => true]
        );
        $this->updateEntityWithNoValidData(
            sprintf('%s%s', $url, $externalId),
            $this->getRootProperty(),
            ['name' => '']
        );
        $this->updateEntityWithNoValidData(
            sprintf('%s%s', $url, '000'),
            $this->getRootProperty(),
            ['name' => 'TEST'],
            Response::HTTP_NOT_FOUND
        );
    }

    public function testDelete()
    {
        $url = '/api/student/delete/';
        $this->logIn();
        for ($i = 0; $i < 10; $i++) {
            $externalId = '999' . ($i + 1);
            if ($i == 0) {
                $externalId = 88899;
            }
            $this->deleteEntity(sprintf('%s%s', $url, $externalId), $this->getRootProperty());
        }
        $this->deleteNoIssetEntity(
            sprintf('%s%s', $url, '000'),
            $this->getRootProperty(),
            [],
            Response::HTTP_NOT_FOUND
        );
    }
}