<?php

namespace ApiBundle\Tests\Controller;


use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultControllerTest
 * @package ApiBundle\Tests\Controller
 */
class UserControllerTest extends DefaultControllerTest
{
    /**
     * DisciplineControllerTest constructor.
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->setRootProperty('user');
    }

    public function testView()
    {
        $url = '/api/user/read';
        $this->logIn();

        $this->pageFound($url, $this->getRootProperty());
        $this->pageNotFound(sprintf('%s/%s', $url, 999), $this->getRootProperty());
    }

    public function testCreate()
    {
        $url = '/api/user/create/';
        $this->logIn();
        $requestParam = [];
        $data = [
            'fullname' => 'Пользователь-номер-%s Тестовый Проверочный',
            'email' => 'test_user'
        ];

        for ($i = 0; $i < 10; $i++) {
            $requestParam = [
                'fullname' => sprintf($data['fullname'], $i),
                'email' => sprintf('%s%s@csp.my', $data['email'], $i),
                'externalId' => $i != 0 ? ('999' . ($i + 1)) : 88899,
                'active' => true
            ];
            $this->createEntity($url, $this->getRootProperty(), $requestParam);
        }

        $invalidData = [
            'fullname' => sprintf($data['fullname'], (string)rand(10001, 10002))
        ];
        $this->createNoValidEntity($url, $this->getRootProperty(), $invalidData);
        $this->createNoValidEntity($url, $this->getRootProperty(), $requestParam);
    }

    public function testEditUser()
    {
        $url = '/api/user/update/';
        $this->logIn();
        $externalId = '';
        $data = [
            'fullname' => 'Пользователь-изменённый-%s Тестовый Проверочный',
            'email' => 'changes_user'
        ];

        for ($i = 0; $i < 5; $i++) {
            $externalId = $i != 0 ? ('999' . ($i + 1)) : 88899;
            $requestParam = [
                'fullname' => sprintf($data['fullname'], $i),
                'email' => $data['email'] . $i . '@csp.my',
                'active' => false
            ];
            if ($i == 0) {
                $requestParam['externalId'] = 88899;
            }
            $this->updateEntity(sprintf('%s%s', $url, $externalId), $this->getRootProperty(), $requestParam);
        }
        $this->updateEntityWithNoValidData(
            sprintf('%s%s', $url, $externalId),
            $this->getRootProperty(),
            ['fullname' => true]
        );
        $this->updateEntityWithNoValidData(
            sprintf('%s%s', $url, $externalId),
            $this->getRootProperty(),
            ['fullname' => '']
        );
        $this->updateEntityWithNoValidData(
            sprintf('%s%s', $url, '000'),
            $this->getRootProperty(),
            ['fullname' => 'TEST'],
            Response::HTTP_NOT_FOUND
        );
    }

    public function testDeleteUser()
    {
        $url = '/api/user/delete/';
        $this->logIn();
        for ($i = 0; $i < 10; $i++) {
            $externalId = $i != 0 ? ('999' . ($i + 1)) : 88899;
            $this->deleteEntity(sprintf('%s%s', $url, $externalId), $this->getRootProperty());
        }

        $this->deleteNoIssetEntity(
            sprintf('%s%s', $url, '000'),
            $this->getRootProperty(),
            [],
            Response::HTTP_NOT_FOUND
        );
    }
}
