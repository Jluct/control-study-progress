<?php

namespace ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Client;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class DefaultControllerTest
 * @package ApiBundle\Tests\Controller
 */
class DefaultControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    protected $client;

    private $rootProperty = 'entity';

    public function setUp()
    {
        $this->client = static::createClient();
    }

    /**
     * @param $url
     * @param $property
     * @param array $params
     */
    protected function pageFound($url, $property, array $params = [])
    {
        $this->client->request('GET', $url, $params);
        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $response = new \SimpleXMLElement($this->client->getResponse()->getContent());
        $this->assertTrue((bool)((int)$response->status));
        $this->assertNotEmpty($response->body->$property);
        $this->assertEmpty($response->errors);
    }

    /**
     * @param $url
     * @param $property
     * @param array $params
     */
    protected function pageNotFound($url, $property, array $params = [])
    {
        $this->client->request('GET', $url, $params);
        $this->assertSame(Response::HTTP_NOT_FOUND, $this->client->getResponse()->getStatusCode());
        $response = new \SimpleXMLElement($this->client->getResponse()->getContent());
        $this->assertEmpty($response->body->$property);
        $this->assertNotEmpty($response->errors);
        $this->assertFalse((bool)((int)$response->status));
    }

    /**
     * @param $url
     * @param $property
     * @param array $params
     */
    protected function createEntity($url, $property, array $params = [])
    {
        $this->client->request('POST', $url, $params);
        $this->noHasError($property);
    }

    /**
     * @param $url
     * @param $property
     * @param array $params
     */
    protected function createNoValidEntity($url, $property, array $params = [])
    {
        $this->client->request('POST', $url, $params);
        $this->hasError($property);
    }

    /**
     * @param $url
     * @param $property
     * @param array $params
     */
    protected function updateEntity($url, $property, array $params = [])
    {
        $this->client->request('PUT', $url, $params);
        $this->noHasError($property);
    }

    /**
     * @param $url
     * @param $property
     * @param array $params
     * @param int $status
     */
    protected function updateEntityWithNoValidData($url, $property, array $params = [], $status = 400)
    {
        $this->client->request('PUT', $url, $params);
        $this->hasError($property, $status);
    }

    /**
     * @param $url
     * @param $property
     * @param array $params
     * @param int $status
     */
    protected function deleteEntity($url, $property, array $params = [], $status = 200)
    {
        $this->client->request('DELETE', $url, $params);
        $this->noHasError($property, $status);
    }

    /**
     * @param $url
     * @param $property
     * @param array $params
     * @param int $status
     */
    protected function deleteNoIssetEntity($url, $property, array $params = [], $status = 400)
    {
        $this->client->request('DELETE', $url, $params);
        $this->hasError($property, $status);
    }

    protected function logIn()
    {
        $session = $this->client->getContainer()->get('session');

        $firewallName = 'api';
        // if you don't define multiple connected firewalls, the context defaults to the firewall name
        // See https://symfony.com/doc/current/reference/configuration/security.html#firewall-context
        $firewallContext = 'api';

        // you may need to use a different token class depending on your application.
        // for example, when using Guard authentication you must instantiate PostAuthenticationGuardToken
        $token = new UsernamePasswordToken('api_user', null, $firewallName, ['ROLE_API']);
        $session->set('_security_' . $firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }

    /**
     * @return string
     */
    protected function getRootProperty()
    {
        return $this->rootProperty;
    }

    /**
     * @param string $rootProperty
     */
    protected function setRootProperty($rootProperty)
    {
        $this->rootProperty = $rootProperty;
    }

    /**
     * @param $property
     * @param int $status
     */
    private function hasError($property, $status = 400)
    {
        $this->assertSame($status, $this->client->getResponse()->getStatusCode());
        $response = new \SimpleXMLElement($this->client->getResponse()->getContent());
        $this->assertFalse((bool)((int)$response->status));
        $this->assertNotEmpty($response->errors);
        $this->assertEquals(1, count($response->body->$property->errors));
    }

    /**
     * @param $property
     * @param int $status
     */
    private function noHasError($property, $status = 200)
    {
        $this->assertSame($status, $this->client->getResponse()->getStatusCode());
        $response = new \SimpleXMLElement($this->client->getResponse()->getContent());
        $this->assertTrue((bool)((int)$response->status));
        $this->assertEmpty($response->errors);
        $this->assertEmpty($response->body->$property->errors);
    }
}