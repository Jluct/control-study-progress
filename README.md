Control Schedule Panel
========================

This project powered by Symfony 3.4 (LTS)

Docker-compose
------

```
docker-compose up
```

after run DB part. If need dump run:

```
php bin/console doctrine:database:import ./app/Resources/files/data/tmp/csp.sql
```

DB
------

```
    php bin/console doctrine:database:create

    php bin/console doctrine:migrations:migrate
```

Fixtures
------

```
    php bin/console doctrine:fixtures:load
```

Test
------

```
    sh ./vendor/bin/simple-phpunit
```

Create user
------

User role:

* 'api' => 'ROLE_API',
* 'user' => 'ROLE_USER',
* 'teacher' => 'ROLE_TEACHER',
* 'moder' => 'ROLE_MODER',
* 'admin' => 'ROLE_ADMIN'

```  
   php bin/console ...
          user:create -l admin -f administrator -p 123456 -m admin1@mail.ru -a true -r admin
          * OR
          user:create --login=admin --fullname=admin --password=123456 --email=admin@mail.ru --active=true --role=admin
```

Assets
------

```
    yarn run encore dev
 
    yarn run encore dev --watch
 
    yarn run encore production
```

Upload data
------

```
    php bin/console csp:data:upload -p /data/d.xlsx
```

Upload teacher
------
```
    php bin/console csp:data:teacher -p /data/tmp/schedule.xlsx
```

Create relation teacher and discipline
------

```
    php bin/console csp:data:relations -p /data/tmp/schedule.xlsx
```

Auth for API example
------

```
    curl -X POST http://csp.my/api/login_check -O --data "_username=userAPI&_password=123456" -D headers

    curl -H "X-AUTH-TOKEN: TOKEN_FOR_PREV_REQUEST" -O http://csp.my/api/user/read -D headers
```
