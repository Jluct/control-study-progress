#!/usr/bin/env bash

cp ./images/php/apache2.conf.tpl ./images/php/config/apache2.conf
cp ./images/php/apt.conf.tpl ./images/php/config/apt.conf
cp ./images/php/curlDownload.sh.tpl ./images/php/config/curlDownload.sh
cp ./images/php/gitconfig.tpl ./images/php/config/gitconfig
touch ./.env