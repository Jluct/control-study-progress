<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20191008133316 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE study_relation (id INT AUTO_INCREMENT NOT NULL, teacher_id INT DEFAULT NULL, discipline_id INT DEFAULT NULL, study_group_id INT DEFAULT NULL, note_type_id INT DEFAULT NULL, externalId VARCHAR(32) DEFAULT NULL, INDEX IDX_C10052C041807E1D (teacher_id), INDEX IDX_C10052C0A5522701 (discipline_id), INDEX IDX_C10052C05DDDCCCE (study_group_id), INDEX IDX_C10052C044EA4809 (note_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE active_history_study_relation (id INT AUTO_INCREMENT NOT NULL, relation_id INT DEFAULT NULL, status TINYINT(1) NOT NULL, createdAt DATETIME NOT NULL, INDEX IDX_89D3D5E13256915B (relation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE department (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, externalId VARCHAR(32) NOT NULL, UNIQUE INDEX UNIQ_CD1DE18AA770AC6E (externalId), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE discipline (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, externalId VARCHAR(32) NOT NULL, is_active TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE group_note (id INT AUTO_INCREMENT NOT NULL, group_id INT DEFAULT NULL, user_id INT DEFAULT NULL, text VARCHAR(1000) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_E6E7A164FE54D947 (group_id), INDEX IDX_E6E7A164A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE note_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE specialty (id INT AUTO_INCREMENT NOT NULL, department_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, externalId VARCHAR(32) NOT NULL, is_active TINYINT(1) DEFAULT NULL, UNIQUE INDEX UNIQ_E066A6ECA770AC6E (externalId), INDEX IDX_E066A6ECAE80F5DF (department_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE student (id INT AUTO_INCREMENT NOT NULL, study_group_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, externalId VARCHAR(32) NOT NULL, UNIQUE INDEX UNIQ_B723AF33A770AC6E (externalId), INDEX IDX_B723AF335DDDCCCE (study_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE student_note (id INT AUTO_INCREMENT NOT NULL, student_id INT DEFAULT NULL, relation_id INT DEFAULT NULL, note VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_F09E81CCCB944F1A (student_id), INDEX IDX_F09E81CC3256915B (relation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE study_group (id INT AUTO_INCREMENT NOT NULL, specialty_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, externalId VARCHAR(32) NOT NULL, is_active TINYINT(1) DEFAULT NULL, UNIQUE INDEX UNIQ_32BA14255E237E06 (name), UNIQUE INDEX UNIQ_32BA1425A770AC6E (externalId), INDEX IDX_32BA14259A353316 (specialty_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, login VARCHAR(30) NOT NULL, fullname VARCHAR(255) NOT NULL, role LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', password VARCHAR(64) NOT NULL, email VARCHAR(60) DEFAULT NULL, salt VARCHAR(32) NOT NULL, is_active TINYINT(1) DEFAULT NULL, external_id VARCHAR(32) DEFAULT NULL, api_key VARCHAR(255) DEFAULT NULL, created_api_key DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649AA08CB10 (login), UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D6499F75D7B0 (external_id), UNIQUE INDEX UNIQ_8D93D649C912ED9D (api_key), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE study_relation ADD CONSTRAINT FK_C10052C041807E1D FOREIGN KEY (teacher_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE study_relation ADD CONSTRAINT FK_C10052C0A5522701 FOREIGN KEY (discipline_id) REFERENCES discipline (id)');
        $this->addSql('ALTER TABLE study_relation ADD CONSTRAINT FK_C10052C05DDDCCCE FOREIGN KEY (study_group_id) REFERENCES study_group (id)');
        $this->addSql('ALTER TABLE study_relation ADD CONSTRAINT FK_C10052C044EA4809 FOREIGN KEY (note_type_id) REFERENCES note_type (id)');
        $this->addSql('ALTER TABLE active_history_study_relation ADD CONSTRAINT FK_89D3D5E13256915B FOREIGN KEY (relation_id) REFERENCES study_relation (id)');
        $this->addSql('ALTER TABLE group_note ADD CONSTRAINT FK_E6E7A164FE54D947 FOREIGN KEY (group_id) REFERENCES study_group (id)');
        $this->addSql('ALTER TABLE group_note ADD CONSTRAINT FK_E6E7A164A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE specialty ADD CONSTRAINT FK_E066A6ECAE80F5DF FOREIGN KEY (department_id) REFERENCES department (id)');
        $this->addSql('ALTER TABLE student ADD CONSTRAINT FK_B723AF335DDDCCCE FOREIGN KEY (study_group_id) REFERENCES study_group (id)');
        $this->addSql('ALTER TABLE student_note ADD CONSTRAINT FK_F09E81CCCB944F1A FOREIGN KEY (student_id) REFERENCES student (id)');
        $this->addSql('ALTER TABLE student_note ADD CONSTRAINT FK_F09E81CC3256915B FOREIGN KEY (relation_id) REFERENCES study_relation (id)');
        $this->addSql('ALTER TABLE study_group ADD CONSTRAINT FK_32BA14259A353316 FOREIGN KEY (specialty_id) REFERENCES specialty (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE active_history_study_relation DROP FOREIGN KEY FK_89D3D5E13256915B');
        $this->addSql('ALTER TABLE student_note DROP FOREIGN KEY FK_F09E81CC3256915B');
        $this->addSql('ALTER TABLE specialty DROP FOREIGN KEY FK_E066A6ECAE80F5DF');
        $this->addSql('ALTER TABLE study_relation DROP FOREIGN KEY FK_C10052C0A5522701');
        $this->addSql('ALTER TABLE study_relation DROP FOREIGN KEY FK_C10052C044EA4809');
        $this->addSql('ALTER TABLE study_group DROP FOREIGN KEY FK_32BA14259A353316');
        $this->addSql('ALTER TABLE student_note DROP FOREIGN KEY FK_F09E81CCCB944F1A');
        $this->addSql('ALTER TABLE study_relation DROP FOREIGN KEY FK_C10052C05DDDCCCE');
        $this->addSql('ALTER TABLE group_note DROP FOREIGN KEY FK_E6E7A164FE54D947');
        $this->addSql('ALTER TABLE student DROP FOREIGN KEY FK_B723AF335DDDCCCE');
        $this->addSql('ALTER TABLE study_relation DROP FOREIGN KEY FK_C10052C041807E1D');
        $this->addSql('ALTER TABLE group_note DROP FOREIGN KEY FK_E6E7A164A76ED395');
        $this->addSql('DROP TABLE study_relation');
        $this->addSql('DROP TABLE active_history_study_relation');
        $this->addSql('DROP TABLE department');
        $this->addSql('DROP TABLE discipline');
        $this->addSql('DROP TABLE group_note');
        $this->addSql('DROP TABLE note_type');
        $this->addSql('DROP TABLE specialty');
        $this->addSql('DROP TABLE student');
        $this->addSql('DROP TABLE student_note');
        $this->addSql('DROP TABLE study_group');
        $this->addSql('DROP TABLE user');
    }
}
