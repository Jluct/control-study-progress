<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\ActiveHistoryStudyRelation;
use AppBundle\Entity\EntityInterface\ActiveHistoryInterface;
use AppBundle\Entity\EntityInterface\LeadingActivityHistory;
use AppBundle\Entity\ReportSetting;
use AppBundle\Entity\StudyRelation;
use AppBundle\Form\Admin\ReportSettingType;
use AppBundle\Form\Admin\StudyRelationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminController
 * @package AppBundle\Controller\
 */
class AdminController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $groupRepository = $this->getDoctrine()->getRepository('AppBundle\Entity\StudyGroup');
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $groupRepository->getGroupQuery($request->query->get('search')),
            $request->query->getInt('page', 1),
            $this->getParameter('pagination.elements_count')
        );

        return $this->render('admin/index.html.twig', [
            'pagination' => $pagination,
            'search' => $request->query->get('search')
        ]);
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function reportAction($id)
    {
        return $this->render('admin/excel.xls.twig', [
            'data' => $this->get('data_group_service')->loadData($id),
            'teacher' => null
        ]);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewGroupAction($id, Request $request)
    {
        $d = $this->getDoctrine();
        $paginator = $this->get('knp_paginator');
        $groupNotesQuery = $d->getRepository('AppBundle:GroupNote')->getNoteByGroupQuery($id);
        $pagination = $paginator->paginate(
            $groupNotesQuery,
            $request->query->getInt('page', 1),
            $this->getParameter('pagination.elements_count')
        );

        $note_csrf = $this->get('security.csrf.token_manager')->getToken('group-note');
        $csrf = $this->get('security.csrf.token_manager')->getToken('check-list');

        return $this->render('default/group/check-list.html.twig', [
            'data' => $this->get('data_group_service')->loadData($id),
            'teacher' => null,
            'csrf' => $csrf,
            'id' => $id,
            'pagination' => $pagination,
            'note_csrf' => $note_csrf,
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function readRelationGroupWithTeacherAction(Request $request)
    {
        $studyRelationRepository = $this->getDoctrine()->getRepository('AppBundle\Entity\StudyRelation');
        $paginator = $this->get('knp_paginator');

        if ($request->query->get('search')) {
            $searchRequest = $studyRelationRepository->setParamsQuery(
                $studyRelationRepository->getStudyRelationQuery(),
                $request->query->get('search')
            );
        } else {
            $searchRequest = $studyRelationRepository->getStudyRelationQuery();
        }
        $searchRequest = $studyRelationRepository->joinActiveHistory($searchRequest);
        $pagination = $paginator->paginate(
            $searchRequest->getQuery(),
            $request->query->getInt('page', 1),
            $this->getParameter('pagination.elements_count')
        );

        return $this->render('admin/study-relation/index.html.twig', [
            'pagination' => $pagination,
            'search' => $request->query->get('search')
        ]);
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteRelationGroupWithTeacherAction($id)
    {
        $studyRelation = $this->getDoctrine()
            ->getRepository('AppBundle:StudyRelation')
            ->findOneBy(['id' => $id]);
        $this->addActiveHistory(0, $studyRelation, (new ActiveHistoryStudyRelation()));
        $this->getDoctrine()->getManager()->persist($studyRelation);
        try {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Data as save');
        } catch (\Exception $e) {
            $this->addFlash('danger', 'Data not save');
        }

        return $this->redirect($this->generateUrl('app_admin_read_group_teacher_relation'));
    }

    /**
     * @param Request $request
     * @param null $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createRelationGroupWithTeacherAction(Request $request, $id = null)
    {
        if (!empty($id)) {
            $studyRelation = $this->getDoctrine()
                ->getRepository('AppBundle:StudyRelation')
                ->findOneBy(['id' => $id]);
        } else {
            $studyRelation = new StudyRelation();
        }
        $form = $this->createForm(StudyRelationType::class, $studyRelation, []);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if (!$form->isValid()) {
                $errors = $form->getErrors(true);
                foreach ($errors as $item) {
                    $this->addFlash(
                        'danger',
                        $item->getMessage()
                    );
                }

                return $this->redirect($this->generateUrl('app_admin_create_group_teacher_relation'));
            }
            $entityManager = $this->getDoctrine()->getManager();
            if (empty($id)) {
                $this->addActiveHistory(1, $studyRelation, (new ActiveHistoryStudyRelation()));
            }
            $entityManager->persist($studyRelation);
            try {
                $entityManager->flush();
                $this->addFlash(
                    'success',
                    'ok'
                );
            } catch (\Exception $e) {
                $this->addFlash(
                    'danger',
                    'error'
                );
            }

            return $this->redirect($this->generateUrl('app_admin_create_group_teacher_relation'));
        }

        return $this->render('admin/create_relation.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fullReportAction(Request $request)
    {
        $students = [];
        $credits = [];
        $averageValue = [
            'students' => 0,
            'small' => 0,
            'long' => 0,
            'lessonShort' => 0,
            'lessonLong' => 0
        ];
        $groupsPatterns = $this->get('groups_years_service')->groupsByYears();
        foreach ($groupsPatterns as $item) {
            $students[] = $this->getDoctrine()
                ->getRepository('AppBundle:Student')
                ->getStudentsByGroups($item);
            $averageValue['students'] += $students[count($students) - 1];
        }
        $reportService = $this->get('report_service');
        $reportSetting = new ReportSetting();
        $form = $this->createForm(ReportSettingType::class, $reportSetting);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $reportService->setBeginDate(
                $reportSetting->getBeginDate()->format('Y.m.d')
            );
            $reportService->setEndDate(
                $reportSetting->getEndDate()->format('Y.m.d')
            );
        }
        foreach ($groupsPatterns as $key => $year) {
            $tmp['total'] = $reportService->getTrainingStreamCredits('< 3', $year);
            $tmp['percent'] = $reportService->creditsByPercent($tmp['total'], $students[$key]);
            $credits['small'][] = $tmp;
            $averageValue['small'] += $tmp['total'];
        }
        foreach ($groupsPatterns as $key => $year) {
            $tmp['total'] = $reportService->getTrainingStreamCredits('>= 3', $year);
            $tmp['percent'] = $reportService->creditsByPercent($tmp['total'], $students[$key]);
            $credits['long'][] = $tmp;
            $averageValue['long'] += $tmp['total'];
        }
        foreach ($groupsPatterns as $key => $year) {
            $fullCredits = $reportService->getLessonStreamCredits('< 20', $year);
            $tmp['total'] = $reportService->getLessonCredits($fullCredits);
            $tmp['percent'] = $reportService->creditsByPercent($tmp['total'], $students[$key]);
            $credits['lessonShort'][] = $tmp;
            $averageValue['lessonShort'] += $tmp['total'];
        }
        foreach ($groupsPatterns as $key => $year) {
            $fullCredits = $reportService->getLessonStreamCredits(' >=20 or sn.note = 0', $year);
            $tmp['total'] = $reportService->getLessonCredits($fullCredits);
            $tmp['percent'] = $reportService->creditsByPercent($tmp['total'], $students[$key]);
            $credits['lessonLong'][] = $tmp;
            $averageValue['lessonLong'] += $tmp['total'];
        }

        return $this->render('admin/full-report.xls.twig', [
            'students' => $students,
            'credits' => $credits,
            'averageValue' => $averageValue
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function reportSettingAction(Request $request)
    {
        $form = $this->createForm(ReportSettingType::class);

        return $this->render('admin/full-report-setting.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param boolean $active
     * @param LeadingActivityHistory $leadingActivityHistory
     * @param ActiveHistoryInterface $activeHistory
     */
    protected function addActiveHistory($active, LeadingActivityHistory $leadingActivityHistory, ActiveHistoryInterface $activeHistory)
    {
        $activeHistory->setStatus($active);
        $leadingActivityHistory->addActiveHistory($activeHistory);
    }
}
