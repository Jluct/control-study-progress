<?php

namespace AppBundle\Controller;


use AppBundle\Entity\GroupNote;
use AppBundle\Form\GroupNoteType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexAction()
    {
        $url = '/error';
        if ($this->isGranted('ROLE_MODER', $this->getUser()) ||
            $this->isGranted('ROLE_ADMIN', $this->getUser())) {
            $url = $this->generateUrl('app_admin_homepage');
        } else if ($this->isGranted('ROLE_TEACHER', $this->getUser())) {
            $url = $this->generateUrl('app_group');
        }

        return $this->redirect($url);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getGroupsAction()
    {
        $user = $this->getUser();
        $user->getRelations()->count();
        $groups = $this->getDoctrine()
            ->getRepository('AppBundle:StudyRelation')
            ->getGroupsByTeacher($this->getUser());

        return $this->render('default/index.html.twig', ['groups' => $groups]);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function groupCheckListAction($id, Request $request)
    {
        $d = $this->getDoctrine();
        $paginator = $this->get('knp_paginator');
        $groupNotesQuery = $d->getRepository('AppBundle:GroupNote')->getNoteByGroupQuery($id);
        $pagination = $paginator->paginate(
            $groupNotesQuery,
            $request->query->getInt('page', 1),
            $this->getParameter('pagination.elements_count')
        );

        $note_csrf = $this->get('security.csrf.token_manager')->getToken('group-note');
        $csrf = $this->get('security.csrf.token_manager')->getToken('check-list');

        return $this->render('default/group/check-list.html.twig', [
            'data' => $this->get('data_group_service')->loadData($id, $this->getUser()),
            'teacher' => $this->getUser(),
            'id' => $id,
            'pagination' => $pagination,
            'csrf' => $csrf,
            'note_csrf' => $note_csrf
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function saveCheckListAction(Request $request)
    {
        $submittedToken = $request->get('csrf');
        $response = new JsonResponse();
        if (!$this->isCsrfTokenValid('check-list', $submittedToken)) {
            $response->setData([
                'errors' => ['csrf is not valid'],
            ]);
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);

            return $response;
        }
        $data = $request->get('data');
        $studentService = $this->container->get('student_service');
        $studentService->updateNotes($data, $this->getUser());
        $response->setData([
            'errors' => $studentService->getErrors(),
            'csrf' => $this->get('security.csrf.token_manager')->refreshToken('check-list')->getValue()
        ]);

        return $response;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function saveGroupNoteAction(Request $request)
    {
        $groupNote = null;
        $submittedToken = $request->request->get('csrf');
        $response = new JsonResponse();
        $data = $request->get('group_note');
        if (!$this->isCsrfTokenValid('group-note', $submittedToken)) {
            $response->setData([
                'errors' => ['csrf is not valid'],
            ]);
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);

            return $response;
        }
        if (isset($data['id']) && !empty($data['id'])) {
            $groupNote = $this->getDoctrine()
                ->getRepository('AppBundle:GroupNote')
                ->findOneBy(['id' => $data['id']]);
        }
        if (empty($groupNote)) {
            $groupNote = new GroupNote();
            $groupNote->setTeacher($this->getUser());
        }
        $groupNote->setDate();

        $form = $this->createForm(GroupNoteType::class, $groupNote);

        if ($form->submit($data) && !count($form->getErrors())) {
            $this->getDoctrine()->getManager()->persist($groupNote);
            $this->getDoctrine()->getManager()->flush();
        } else {
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        $response->setData([
            'errors' => (string)$form->getErrors(),
            'id' => $groupNote->getId(),
            'date' => $groupNote->getUpdatedAt()->format('H:i d.m.Y'),
            'csrf' => $this->get('security.csrf.token_manager')->refreshToken('group-note')->getValue()
        ]);

        return $response;
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteGroupNoteAction($id, Request $request)
    {
        $submittedToken = $request->request->get('csrf');
        $response = new JsonResponse();
        $errors = [];
        if (!$this->isCsrfTokenValid('group-note', $submittedToken)) {
            $response->setData([
                'errors' => ['csrf is not valid'],
            ]);
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);

            return $response;
        }
        $note = $this->getDoctrine()->getRepository('AppBundle:GroupNote')->findOneBy(['id' => $id]);
        if (empty($note)) {
            throw new NotFoundHttpException();
        } else {
            try {
                $this->getDoctrine()->getManager()->remove($note);
                $this->getDoctrine()->getManager()->flush();
            } catch (\Exception $e) {
                $errors[] = $e->getMessage();
            }
        }

        $response->setData([
            'errors' => $errors,
            'csrf' => $this->get('security.csrf.token_manager')->refreshToken('group-note')->getValue()
        ]);

        return $response;
    }
}
