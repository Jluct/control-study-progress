<?php

namespace AppBundle\Form\Admin;


use AppBundle\Entity\Discipline;
use AppBundle\Entity\NoteType;
use AppBundle\Entity\StudyGroup;
use AppBundle\Repository\NoteTypeRepository;
use AppBundle\Repository\StudyGroupRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Entity\User;
use AppBundle\Repository\DisciplineRepository;

/**
 * Class StudyRelationType
 * @package AppBundle\Form
 */
class StudyRelationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', HiddenType::class, [
                'required' => false
            ])
            ->add('teacher', EntityType::class, [
                'class' => User::class,
                'choice_label' => 'fullname'
            ])
            ->add('discipline', EntityType::class, [
                'class' => Discipline::class,
                'query_builder' => function (DisciplineRepository $er) {
                    return $er->createQueryBuilder('d')->where('d.isActive = 1');
                },
                'choice_label' => 'name'
            ])
            ->add('studyGroup', EntityType::class, [
                'class' => StudyGroup::class,
                'query_builder' => function (StudyGroupRepository $er) {
                    return $er->createQueryBuilder('g')->where('g.isActive = 1');
                },
                'choice_label' => 'name'
            ])
            ->add('noteType', EntityType::class, [
                'class' => NoteType::class,
                'query_builder' => function (NoteTypeRepository $er) {
                    return $er->createQueryBuilder('n');
                },
                'choice_label' => 'name'
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\StudyRelation'
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_studyrelation';
    }
}