<?php

namespace AppBundle\Services;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\StudentNote;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use UserBundle\Entity\User;

/**
 * Class StudentService
 * @package AppBundle\Services
 */
class StudentService
{
    /**
     * @var EntityManager
     */
    private $manager;

    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    /**
     * @var array
     */
    private $notes;

    /**
     * @var array
     */
    private $errors = [];

    /**
     * StudentService constructor.
     * @param EntityManager $manager
     * @param AuthorizationChecker authorizationChecker
     */
    public function __construct(EntityManager $manager, AuthorizationChecker $authorizationChecker)
    {
        $this->manager = $manager;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param array $data
     * @param User $teacher
     */
    public function updateNotes(array $data, User $teacher)
    {
        $this->notes = $this->loadExistNotes($data, $teacher->getId());
        foreach ($data as $item) {
            $this->manager->persist($this->findNotes($item, $teacher));
        }
        try {
            $this->manager->flush();
        } catch (\Exception $e) {
            $this->errors[] = $e->getMessage();
        }
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $data
     * @param User $teacher
     * @return StudentNote
     */
    private function findNotes(array $data, User $teacher)
    {
        /** @var StudentNote $item */
        foreach ($this->notes as $item) {
            if (
                $item->getStudent()->getId() == $data['student'] &&
                $item->getStudyRelation()->getDiscipline()->getId() == $data['discipline'] &&
                $item->getNoteType()->getId() == $data['type']
            ) {
                return $this->updateStudentNote($data, $item);
            } else {
                $data['relation'] = $item->getStudyRelation();
            }
        }

        return $this->createStudentNote($data, $teacher);
    }

    /**
     * @param array $data
     * @param StudentNote $note
     * @return StudentNote
     */
    public function updateStudentNote(array $data, StudentNote $note)
    {
        $note->setNote($data['val']);
        $note->setDate();

        return $note;
    }

    /**
     * @param array $data
     * @param User $teacher
     * @return StudentNote
     * @throws \Exception
     */
    private function createStudentNote(array $data, User $teacher)
    {
        $studentNote = new StudentNote();
        $studyRelation = $this->manager->getRepository('AppBundle\Entity\StudyRelation')
            ->findOneBy(['id' => $data['relation']]);
        if (!$this->authorizationChecker->isGranted('ROLE_MODER')) {
            if ($studyRelation->getTeacher()->getId() != $teacher->getId()) {
                throw new \Exception('Data do not match');
            }
        }
        $student = $this->manager->getRepository('AppBundle\Entity\Student')
            ->findOneBy(['id' => $data['student']]);
        $studentNote->setNote($data['val']);
        $studentNote->setStudent($student);
        $studentNote->setStudyRelation($studyRelation);

        return $studentNote;
    }

    /**
     * @param array $data
     * @param integer $userId
     * @return StudentNote[]|array
     */
    private function loadExistNotes(array $data, $userId)
    {
        $studyRelations = $this->manager->getRepository('AppBundle:StudyRelation')
            ->findBy([
                'teacher' => $userId,
                'discipline' => array_column($data, 'discipline'),
                'studyGroup' => $data[0]['group']
            ]);

        return $this->manager
            ->getRepository('AppBundle\Entity\StudentNote')
            ->findBy([
                'student' => array_column($data, 'student'),
                'studyRelation' => array_column($studyRelations, 'id'),
            ]);
    }
}