<?php

namespace AppBundle\Services;

/**
 * Class GroupsYearsService
 * @package AppBundle\Services
 */
class GroupsYearsService
{
    CONST COURSE_COUNT = 4;

    /**
     * @return array
     */
    public function groupsByYears()
    {
        $groups = [];
        $year = date('y');
        if (date('m') <= 9) {
            $year = date('y') - 1;
        }
        $yearNumber = substr($year, -1);
        for ($i = 0; $i < self::COURSE_COUNT; $i++) {
            if ($yearNumber - $i < 0) {
                $yearNumber = 9 + $i;
            }
            $groups[] = $yearNumber - $i;
        }

        return $groups;
    }
}