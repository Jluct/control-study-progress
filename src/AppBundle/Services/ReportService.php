<?php


namespace AppBundle\Services;


use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ReportService
 * @package AppBundle\Services
 */
class ReportService
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var string
     */
    private $semesterBegin;

    /**
     * @var string
     */
    private $semesterEnd;

    /**
     * @var string
     */
    private $lessonNoteType;

    /**
     * @var string
     */
    private $beginDate;

    /**
     * @var string
     */
    private $endDate;

    /**
     * DataGroupService constructor.
     * @param EntityManagerInterface $manager
     * @param $semesterBegin
     * @param $semesterEnd
     * @param $lessonNoteType
     */
    public function __construct(EntityManagerInterface $manager, $semesterBegin, $semesterEnd, $lessonNoteType)
    {
        $this->manager = $manager;
        $this->semesterBegin = $semesterBegin;
        $this->semesterEnd = $semesterEnd;
        $this->lessonNoteType = $lessonNoteType;
    }

    /**
     * @param string $condition
     * @param string $year
     * @return mixed|null
     */
    public function getTrainingStreamCredits($condition, $year)
    {
        $params = [
            'year' => $year,
            'begin' => $this->semesterBegin,
            'end' => $this->semesterEnd,
            'noteType' => $this->lessonNoteType
        ];
        $studentRepository = $this->manager
            ->getRepository('AppBundle:Student');
        $subQuery = $studentRepository->createNoteQuery($condition);
        if (!empty($this->beginDate) && !empty($this->endDate)) {
            $subQuery = $studentRepository->addPeriod($subQuery);
            $params['beginDate'] = $this->beginDate;
            $params['endDate'] = $this->endDate;
        }
        $subQuery = $studentRepository->addNotLikeCondition($subQuery);
        $subQuery = $studentRepository->addTimeCondition($subQuery);
        $subQuery = $studentRepository->addActiveRelationCondition($subQuery);
        $subQuery = $studentRepository->addYearCondition($subQuery);
        $subQuery = $studentRepository->addNoteCondition($subQuery);
        $query = $studentRepository->getStudentsCredits($subQuery);
        $query = $studentRepository->addParams(
            $query,
            $params
        );

        return $studentRepository->runStudentCreditsQuery($query);
    }

    /**
     * @param $condition
     * @param $year
     * @return mixed|null
     */
    public function getLessonStreamCredits($condition, $year)
    {
        $params = [
            'year' => $year,
            'begin' => $this->semesterBegin,
            'end' => $this->semesterEnd,
            'noteType' => $this->lessonNoteType
        ];
        $studentRepository = $this->manager
            ->getRepository('AppBundle:Student');
        $query = $studentRepository->getLessonCredits();
        if (!empty($this->beginDate) && !empty($this->endDate)) {
            $query = $studentRepository->addPeriod($query);
            $params['beginDate'] = $this->beginDate;
            $params['endDate'] = $this->endDate;
        }
        $query = $studentRepository->joinLikeCondition($query);
        $query = $studentRepository->addTimeCondition($query);
        $query = $studentRepository->addActiveRelationCondition($query);
        $query = $studentRepository->addYearCondition($query);
        $query = $studentRepository->addNoteCondition($query, $condition);
        $query = $studentRepository->addParams(
            $query,
            $params
        );

        return $studentRepository->runLessonCreditsQuery($query);
    }

    /**
     * @param $dept
     * @param $students
     * @return float
     */
    public function creditsByPercent($dept, $students)
    {
        return round(100 * $dept / $students, 2);
    }

    /**
     * @param array $notes
     * @return integer
     */
    public function getLessonCredits(array $notes)
    {
        $data = [];
        foreach ($notes as $note) {
            if (isset($data[$note->getStudent()->getId()]) &&
                $data[$note->getStudent()->getId()]['updatedAt'] > $note->getUpdatedAt()) {
                continue;
            }
            $tmp = [];
            $tmp['note'] = $note->getNote();
            $tmp['updatedAt'] = $note->getUpdatedAt();
            $data[$note->getStudent()->getId()] = $tmp;
        }
        $data = array_filter($data, function ($item) {
            return $item['note'] == true;
        });

        return count($data);
    }

    /**
     * @return string
     */
    public function getBeginDate()
    {
        return $this->beginDate;
    }

    /**
     * @param string $beginDate
     */
    public function setBeginDate($beginDate)
    {
        $this->beginDate = $beginDate;
    }

    /**
     * @return string
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param string $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }
}