<?php

namespace AppBundle\Services;


use AppBundle\Entity\StudyRelation;
use Doctrine\ORM\EntityManager;
use UserBundle\Entity\User;

/**
 * Class DataGroupService
 * @package AppBundle\Services
 */
class DataGroupService
{
    /**
     * @var EntityManager
     */
    private $manager;

    /**
     * @var string
     */
    private $semesterBegin;

    /**
     * @var string
     */
    private $semesterEnd;

    /**
     * DataGroupService constructor.
     * @param EntityManager $manager
     * @param $semesterBegin
     * @param $semesterEnd
     */
    public function __construct(EntityManager $manager, $semesterBegin, $semesterEnd)
    {
        $this->manager = $manager;
        $this->semesterBegin = $semesterBegin;
        $this->semesterEnd = $semesterEnd;
    }

    /**
     * @param $id
     * @param User $teacher
     * @return array
     */
    public function loadData($id, User $teacher = null)
    {
        $em = $this->manager;
        $data = [];
        $data['group'] = $em->getRepository('AppBundle:StudyGroup')
            ->findOneBy(['id' => $id, 'isActive' => 1]);

        $relationConditions = [
            'studyGroup' => $id
        ];
        if (!empty($teacher)) {
            $relationConditions['teacher'] = $teacher->getId();
        }
        $relations = $em->getRepository('AppBundle:StudyRelation')
            ->getActualStudyRelation($relationConditions);
        $data['students'] = $em->getRepository('AppBundle:Student')
            ->getStudents($data['group']->getId());
        count($data['students']);
        $data['disciplines'] = $this->makeDisciplines($relations);
        $data['noteType'] = $this->makeNoteType($relations, $teacher);
        $data['notes'] = $em->getRepository('AppBundle\Entity\StudentNote')
            ->getNotesByDiscipline(
                $relations,
                $this->semesterBegin,
                $this->semesterEnd
            );
        $data['notes'] = $this->sortNote($data['notes'], $data['noteType'], $data['students']);

        return $data;
    }

    /**
     * @param array $notes
     * @param array $types
     * @param array $students
     * @return array
     */
    private function sortNote(array $notes, array $types, array $students)
    {
        $data = [];
        foreach ($students as $student) {
            $data[$student->getId()] = [];
            foreach ($types as $type) {
                $data[$student->getId()][$type['relation']] = [
                    'note_id' => null,
                    'note_value' => null,
                    'student_id' => $student->getId(),
                    'relation_id' => $type['relation'],
                    'type_id' => $type['id'],
                    'discipline_id' => $type['discipline_id']
                ];
            }
        }
        foreach ($notes as $note) {
            $data[$note['student_id']][$note['relation_id']] = $note;
        }

        return $data;
    }

    /**
     * @param array $relations
     * @param User $teacher
     * @return array
     */
    private function makeNoteType(array $relations, $teacher = null)
    {
        $data = [];
        /** @var StudyRelation $relation */
        foreach ($relations as $relation) {
            $tmp['name'] = $relation->getNoteType()->getName();
            $tmp['id'] = $relation->getNoteType()->getId();
            $tmp['relation'] = $relation->getId();
            if (empty($teacher)) {
                $fullName = explode(' ', $relation->getTeacher()->getFullName());
                $initials = sprintf('%s %s.', $fullName[0], mb_substr($fullName[1], 0, 1));
                if (isset($fullName[2])) {
                    $initials = sprintf('%s%s.', $initials, mb_substr($fullName[2], 0, 1));
                }
                $tmp['teacher'] = $initials;
            }
            $data[] = $tmp;
        }

        return $data;
    }

    /**
     * @param array $relations
     * @return array
     */
    private function makeDisciplines(array $relations)
    {
        $data = [];
        /** @var StudyRelation $relation */
        foreach ($relations as $relation) {
            if ($relation->getDiscipline()->getName() == $data[count($data) - 1]['discipline']) {
                $data[count($data) - 1]['count']++;
                continue;
            }
            $tmp['discipline'] = $relation->getDiscipline()->getName();
            $tmp['count'] = 1;
            $data[] = $tmp;
        }

        return $data;
    }
}