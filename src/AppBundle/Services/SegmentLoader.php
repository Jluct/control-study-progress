<?php

namespace AppBundle\Services;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;

/**
 * Class SegmentLoader
 * @package AppBundle\Services
 */
class SegmentLoader
{
    /**
     * @var EntityManager
     */
    private $manager;

    /**
     * @var QueryBuilder
     */
    private $query;

    /**
     * StudentService constructor.
     * @param EntityManager $manager
     */
    public function __construct(EntityManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param $repository
     * @param int $offset
     * @param int $limit
     * @return $this
     */
    public function segment($repository, $offset = 1, $limit = 50)
    {
        $this->query = $this->manager->createQueryBuilder()->select('d')->from($repository, 'd')
            ->setMaxResults($limit)
            ->setFirstResult(($offset - 1) * $limit);

        return $this;
    }

    /**
     * @param $condition
     * @param array $params
     * @return $this
     */
    public function where($condition, array $params = [])
    {
        $this->query->where($condition);
        if (empty($params)) {
            return $this;
        }

        foreach ($params as $key => $value) {
            $this->query->setParameter($key, $value);
        }

        return $this;
    }

    /**
     * @param $entity
     * @param $activeHistory
     * @param string $groupBy
     * @param int $offset
     * @param int $limit
     * @return $this
     */
    public function activeHistory($entity, $activeHistory, $groupBy, $offset = 1, $limit = 50)
    {
        $this->query = $this->manager->createQueryBuilder()
            ->select('r', 'ah')
            ->from($entity, 'r')
            ->innerJoin('r.activeHistory', 'ah', 'r.id = ah.studyRelation');

        $this->query->where(
            $this->query->expr()->in(
                'ah.createdAt',
                $this->manager->createQueryBuilder()
                    ->select('MAX(ahsr.createdAt) as createdAt')
                    ->from($activeHistory, 'ahsr')
                    ->groupBy(sprintf('ahsr.%s', $groupBy))
                    ->getDQL()
            )
        )
            ->setMaxResults($limit)
            ->setFirstResult(($offset - 1) * $limit);

        return $this;
    }

    /**
     * @return array|null
     */
    public function load()
    {
        try {
            return $this->query->getQuery()->getResult();
        } catch (\Exception $e) {
            return null;
        }
    }
}