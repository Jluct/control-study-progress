<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Discipline
 *
 * @ORM\Table(name="discipline")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DisciplineRepository")
 * @UniqueEntity(fields={"name", "externalId"})
 */
class Discipline
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min="2", max="255")
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var int
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="externalId", type="string", length=32, nullable=false)
     */
    private $externalId;

    /**
     * @var StudyRelation
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\StudyRelation", mappedBy="discipline")
     */
    private $relations;

    /**
     * @var boolean
     * @ORM\Column(name="is_active", nullable=true, type="boolean")
     */
    private $isActive;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->relations = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Discipline
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set externalId
     *
     * @param integer $externalId
     *
     * @return Discipline
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * Get externalId
     *
     * @return int
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Set relations
     *
     * @param ArrayCollection $relations
     *
     * @return Discipline
     */
    public function setRelations(ArrayCollection $relations = null)
    {
        $this->relations = $relations;

        return $this;
    }

    /**
     * Get relations
     *
     * @return \AppBundle\Entity\StudyRelation
     */
    public function getRelations()
    {
        return $this->relations;
    }

    /**
     * Add relation
     *
     * @param \AppBundle\Entity\StudyRelation $relation
     *
     * @return Discipline
     */
    public function addRelation(\AppBundle\Entity\StudyRelation $relation)
    {
        $this->relations[] = $relation;

        return $this;
    }

    /**
     * Remove relation
     *
     * @param \AppBundle\Entity\StudyRelation $relation
     */
    public function removeRelation(\AppBundle\Entity\StudyRelation $relation)
    {
        $this->relations->removeElement($relation);
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
}
