<?php

namespace AppBundle\Entity\EntityInterface;

/**
 * Interface ActiveHistoryInterface
 * @package AppBundle\Entity\EntityInterface
 */
interface ActiveHistoryInterface
{
    /**
     * @return mixed
     */
    public function getStatus();

    /**
     * @param boolean $status
     * @return boolean
     */
    public function setStatus($status);
}