<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Student
 *
 * @ORM\Table(name="student")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StudentRepository")
 * @UniqueEntity(fields={"name", "externalId"})
 */
class Student
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min="2", max="255")
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="externalId", type="string", length=32, unique=true)
     */
    private $externalId;

    /**
     * @var StudyGroup
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\StudyGroup", inversedBy="students")
     * @ORM\JoinColumn(name="study_group_id", referencedColumnName="id")
     */
    private $studyGroup;

    /**
     * @var StudentNote
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\StudentNote", mappedBy="student")
     */
    private $notes;

    /**
     * Student constructor.
     */
    public function __construct()
    {
        $this->notes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Student
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set externalId
     *
     * @param integer $externalId
     *
     * @return Student
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * Get externalId
     *
     * @return int
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Set studyGroup
     *
     * @param \AppBundle\Entity\StudyGroup $studyGroup
     *
     * @return Student
     */
    public function setStudyGroup(\AppBundle\Entity\StudyGroup $studyGroup = null)
    {
        $this->studyGroup = $studyGroup;

        return $this;
    }

    /**
     * Get studyGroup
     *
     * @return \AppBundle\Entity\StudyGroup
     */
    public function getStudyGroup()
    {
        return $this->studyGroup;
    }

    /**
     * Add note
     *
     * @param \AppBundle\Entity\StudentNote $note
     *
     * @return Student
     */
    public function addNote(\AppBundle\Entity\StudentNote $note)
    {
        $this->notes[] = $note;

        return $this;
    }

    /**
     * Remove note
     *
     * @param \AppBundle\Entity\StudentNote $note
     */
    public function removeNote(\AppBundle\Entity\StudentNote $note)
    {
        $this->notes->removeElement($note);
    }

    /**
     * Get notes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotes()
    {
        return $this->notes;
    }
}
