<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityInterface\ActiveHistoryInterface;
use AppBundle\Entity\EntityInterface\LeadingActivityHistory;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * StudyRelation
 *
 * @UniqueEntity(fields={"teacher", "discipline", "studyGroup" ,"noteType"})
 *
 * @ORM\Table(name="study_relation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StudyRelationRepository")
 */
class StudyRelation implements LeadingActivityHistory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     * @Assert\NotBlank()
     * @Assert\Type(type="UserBundle\Entity\User")
     * @Assert\Valid()
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="relations")
     * @ORM\JoinColumn(name="teacher_id", referencedColumnName="id")
     */
    private $teacher;

    /**
     * @var Discipline
     * @Assert\NotBlank()
     * @Assert\Type(type="AppBundle\Entity\Discipline")
     * @Assert\Valid()
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Discipline", inversedBy="relations")
     * @ORM\JoinColumn(name="discipline_id", referencedColumnName="id")
     */
    private $discipline;

    /**
     * @var StudyGroup
     * @Assert\NotBlank()
     * @Assert\Type(type="AppBundle\Entity\StudyGroup")
     * @Assert\Valid()
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\StudyGroup", inversedBy="relations")
     * @ORM\JoinColumn(name="study_group_id", referencedColumnName="id")
     */
    private $studyGroup;

    /**
     * @var int
     *
     * @ORM\Column(name="externalId", type="string", length=32, nullable=true)
     */
    private $externalId;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ActiveHistoryStudyRelation", mappedBy="studyRelation", cascade={"all"})
     */
    private $activeHistory;

    /**
     * @var StudentNote
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\StudentNote", mappedBy="studyRelation")
     */
    private $notes;

    /**
     * @var NoteType
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\NoteType", inversedBy="studyRelations")
     * @ORM\JoinColumn(name="note_type_id", referencedColumnName="id")
     */
    private $noteType;

    /**
     * StudyRelation constructor.
     */
    public function __construct()
    {
        $this->activeHistory = new ArrayCollection();
        $this->notes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set teacher
     *
     * @param \UserBundle\Entity\User $teacher
     *
     * @return StudyRelation
     */
    public function setTeacher(\UserBundle\Entity\User $teacher = null)
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return \UserBundle\Entity\User
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * Set discipline
     *
     * @param \AppBundle\Entity\Discipline $discipline
     *
     * @return StudyRelation
     */
    public function setDiscipline(\AppBundle\Entity\Discipline $discipline = null)
    {
        $this->discipline = $discipline;

        return $this;
    }

    /**
     * Get discipline
     *
     * @return \AppBundle\Entity\Discipline
     */
    public function getDiscipline()
    {
        return $this->discipline;
    }

    /**
     * Set studyGroup
     *
     * @param \AppBundle\Entity\StudyGroup $studyGroup
     *
     * @return StudyRelation
     */
    public function setStudyGroup(\AppBundle\Entity\StudyGroup $studyGroup = null)
    {
        $this->studyGroup = $studyGroup;

        return $this;
    }

    /**
     * Get studyGroup
     *
     * @return \AppBundle\Entity\StudyGroup
     */
    public function getStudyGroup()
    {
        return $this->studyGroup;
    }

    /**
     * @return int
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param int $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * Add activeHistory
     *
     * @param \AppBundle\Entity\ActiveHistoryStudyRelation $activeHistory
     *
     * @return StudyRelation
     */
    public function addActiveHistory(\AppBundle\Entity\ActiveHistoryStudyRelation $activeHistory)
    {
        $activeHistory->setStudyRelation($this);
        $this->activeHistory[] = $activeHistory;

        return $this;
    }

    /**
     * Remove activeHistory
     *
     * @param \AppBundle\Entity\ActiveHistoryStudyRelation $activeHistory
     */
    public function removeActiveHistory(\AppBundle\Entity\ActiveHistoryStudyRelation $activeHistory)
    {
        $this->activeHistory->removeElement($activeHistory);
    }

    /**
     * Get activeHistory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActiveHistory()
    {
        return $this->activeHistory;
    }

    /**
     * Add note
     *
     * @param \AppBundle\Entity\StudentNote $note
     *
     * @return $this
     */
    public function addNote(\AppBundle\Entity\StudentNote $note)
    {
        $this->notes[] = $note;

        return $this;
    }

    /**
     * Remove note
     *
     * @param \AppBundle\Entity\StudentNote $note
     */
    public function removeNote(\AppBundle\Entity\StudentNote $note)
    {
        $this->notes->removeElement($note);
    }

    /**
     * Get notes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set noteType
     *
     * @param \AppBundle\Entity\NoteType $noteType
     *
     * @return $this
     */
    public function setNoteType(\AppBundle\Entity\NoteType $noteType = null)
    {
        $this->noteType = $noteType;

        return $this;
    }

    /**
     * Get noteType
     *
     * @return \AppBundle\Entity\NoteType
     */
    public function getNoteType()
    {
        return $this->noteType;
    }
}
