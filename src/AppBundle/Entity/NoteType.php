<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NoteType
 *
 * @ORM\Table(name="note_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NoteTypeRepository")
 */
class NoteType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var StudentNote
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\StudyRelation", mappedBy="noteType")
     */
    private $studyRelations;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return NoteType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->notes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add note
     *
     * @param \AppBundle\Entity\StudentNote $note
     *
     * @return NoteType
     */
    public function addNote(\AppBundle\Entity\StudentNote $note)
    {
        $this->notes[] = $note;

        return $this;
    }

    /**
     * Remove note
     *
     * @param \AppBundle\Entity\StudentNote $note
     */
    public function removeNote(\AppBundle\Entity\StudentNote $note)
    {
        $this->notes->removeElement($note);
    }

    /**
     * Get notes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Add studyRelation
     *
     * @param \AppBundle\Entity\StudyRelation $studyRelation
     *
     * @return NoteType
     */
    public function addStudyRelation(\AppBundle\Entity\StudyRelation $studyRelation)
    {
        $studyRelation->setNoteType($this);
        $this->studyRelations[] = $studyRelation;

        return $this;
    }

    /**
     * Remove studyRelation
     *
     * @param \AppBundle\Entity\StudyRelation $studyRelation
     */
    public function removeStudyRelation(\AppBundle\Entity\StudyRelation $studyRelation)
    {
        $this->studyRelations->removeElement($studyRelation);
    }

    /**
     * Get studyRelations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudyRelations()
    {
        return $this->studyRelations;
    }
}
