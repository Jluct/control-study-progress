<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * StudyGroup
 *
 * @ORM\Table(name="study_group")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StudyGroupRepository")
 * @UniqueEntity("name")
 * @UniqueEntity("externalId")
 */
class StudyGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(min="2", max="255")
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var int
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="externalId", type="string", length=32, unique=true)
     */
    private $externalId;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="StudyRelation", mappedBy="studyGroup")
     */
    private $relations;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Student", mappedBy="studyGroup")
     */
    private $students;

    /**
     * @var Specialty
     * @Assert\Type(type="AppBundle\Entity\Specialty")
     * @Assert\Valid()
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Specialty", inversedBy="studyGroups")
     * @ORM\JoinColumn(name="specialty_id", referencedColumnName="id")
     */
    private $specialty;

    /**
     * @var GroupNote
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\GroupNote", mappedBy="group")
     */
    private $notes;

    /**
     * @var boolean
     * @ORM\Column(name="is_active", nullable=true, type="boolean")
     */
    private $isActive;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->relations = new ArrayCollection();
        $this->students = new ArrayCollection();
        $this->notes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return StudyGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set externalId
     *
     * @param integer $externalId
     *
     * @return StudyGroup
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * Get externalId
     *
     * @return int
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Add relation
     *
     * @param \AppBundle\Entity\StudyRelation $relation
     *
     * @return StudyGroup
     */
    public function addRelation(\AppBundle\Entity\StudyRelation $relation)
    {
        $this->relations[] = $relation;

        return $this;
    }

    /**
     * @param ArrayCollection $relations
     * @return $this
     */
    public function setRelations(ArrayCollection $relations)
    {
        $this->relations = $relations;

        return $this;
    }

    /**
     * Remove relation
     *
     * @param \AppBundle\Entity\StudyRelation $relation
     */
    public function removeRelation(\AppBundle\Entity\StudyRelation $relation)
    {
        $this->relations->removeElement($relation);
    }

    /**
     * Get relations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRelations()
    {
        return $this->relations;
    }

    /**
     * Add student
     *
     * @param \AppBundle\Entity\Student $student
     *
     * @return StudyGroup
     */
    public function addStudent(\AppBundle\Entity\Student $student)
    {
        $this->students[] = $student;

        return $this;
    }

    /**
     * Remove student
     *
     * @param \AppBundle\Entity\Student $student
     */
    public function removeStudent(\AppBundle\Entity\Student $student)
    {
        $this->students->removeElement($student);
    }

    /**
     * Get students
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudents()
    {
        return $this->students;
    }

    /**
     * Set specialty
     *
     * @param \AppBundle\Entity\Specialty $specialty
     *
     * @return StudyGroup
     */
    public function setSpecialty(\AppBundle\Entity\Specialty $specialty = null)
    {
        $this->specialty = $specialty;

        return $this;
    }

    /**
     * Get specialty
     *
     * @return \AppBundle\Entity\Specialty
     */
    public function getSpecialty()
    {
        return $this->specialty;
    }

    /**
     * Add note
     *
     * @param \AppBundle\Entity\GroupNote $note
     *
     * @return StudyGroup
     */
    public function addNote(\AppBundle\Entity\GroupNote $note)
    {
        $this->notes[] = $note;

        return $this;
    }

    /**
     * Remove note
     *
     * @param \AppBundle\Entity\GroupNote $note
     */
    public function removeNote(\AppBundle\Entity\GroupNote $note)
    {
        $this->notes->removeElement($note);
    }

    /**
     * Get notes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
}
