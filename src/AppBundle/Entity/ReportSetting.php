<?php

namespace AppBundle\Entity;


use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ReportSetting
 * @package AppBundle\Entity
 */
class ReportSetting
{
    /**
     * @var \DateTime
     * @Assert\NotBlank
     */
    private $beginDate;

    /**
     * @var \DateTime
     * @Assert\NotBlank
     */
    private $endDate;

    /**
     * @return \DateTime
     */
    public function getBeginDate()
    {
        return $this->beginDate;
    }

    /**
     * @param \DateTime $beginDate
     */
    public function setBeginDate($beginDate)
    {
        $this->beginDate = $beginDate;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }
}