<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Specialty
 *
 * @ORM\Table(name="specialty")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SpecialtyRepository")
 * @UniqueEntity(fields={"name", "externalId"})
 */
class Specialty
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\Length(min="2", max="255")
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="externalId", type="string", length=32, unique=true)
     */
    private $externalId;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\StudyGroup", mappedBy="specialty")
     */
    private $studyGroups;

    /**
     * @var Department
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Department", inversedBy="specialties")
     * @ORM\JoinColumn(name="department_id", referencedColumnName="id")
     */
    private $department;

    /**
     * @var boolean
     * @ORM\Column(name="is_active", nullable=true, type="boolean")
     */
    private $isActive;

    /**
     * Specialty constructor.
     */
    public function __construct()
    {
        $this->studyGroups = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Specialty
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set externalId
     *
     * @param integer $externalId
     *
     * @return Specialty
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * Get externalId
     *
     * @return int
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Add studyGroup
     *
     * @param \AppBundle\Entity\StudyGroup $studyGroup
     *
     * @return Specialty
     */
    public function addStudyGroup(\AppBundle\Entity\StudyGroup $studyGroup)
    {
        $this->studyGroups[] = $studyGroup;

        return $this;
    }

    /**
     * Remove studyGroup
     *
     * @param \AppBundle\Entity\StudyGroup $studyGroup
     */
    public function removeStudyGroup(\AppBundle\Entity\StudyGroup $studyGroup)
    {
        $this->studyGroups->removeElement($studyGroup);
    }

    /**
     * Get studyGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStudyGroups()
    {
        return $this->studyGroups;
    }

    /**
     * Set department
     *
     * @param \AppBundle\Entity\Department $department
     *
     * @return $this
     */
    public function setDepartment(\AppBundle\Entity\Department $department = null)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return \AppBundle\Entity\Department
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
}
