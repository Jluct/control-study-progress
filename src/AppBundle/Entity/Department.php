<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Department
 *
 * @ORM\Table(name="department")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DepartmentRepository")
 */
class Department
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="externalId", type="string", length=32, unique=true)
     */
    private $externalId;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Specialty", mappedBy="department")
     */
    private $specialties;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->specialties = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Department
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set externalId
     *
     * @param integer $externalId
     *
     * @return Department
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * Get externalId
     *
     * @return int
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Add specialty
     *
     * @param \AppBundle\Entity\Specialty $specialty
     *
     * @return Department
     */
    public function addSpecialty(\AppBundle\Entity\Specialty $specialty)
    {
        $this->specialties[] = $specialty;

        return $this;
    }

    /**
     * Remove specialty
     *
     * @param \AppBundle\Entity\Specialty $specialty
     */
    public function removeSpecialty(\AppBundle\Entity\Specialty $specialty)
    {
        $this->specialties->removeElement($specialty);
    }

    /**
     * Get specialties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpecialties()
    {
        return $this->specialties;
    }
}
