<?php

namespace AppBundle\Entity;

use AppBundle\Entity\EntityInterface\ActiveHistoryInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * ActiveHistoryStudyRelation
 *
 * @ORM\Table(name="active_history_study_relation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ActiveHistoryStudyRelationRepository")
 */
class ActiveHistoryStudyRelation implements ActiveHistoryInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var StudyGroup
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\StudyRelation", inversedBy="activeHistory")
     * @ORM\JoinColumn(name="relation_id", referencedColumnName="id")
     */
    private $studyRelation;

    /**
     * ActiveHistoryStudyRelation constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return ActiveHistoryStudyRelation
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ActiveHistoryStudyRelation
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set studyRelation
     *
     * @param \AppBundle\Entity\StudyRelation $studyRelation
     *
     * @return ActiveHistoryStudyRelation
     */
    public function setStudyRelation(\AppBundle\Entity\StudyRelation $studyRelation = null)
    {
        $this->studyRelation = $studyRelation;

        return $this;
    }

    /**
     * Get studyRelation
     *
     * @return \AppBundle\Entity\StudyRelation
     */
    public function getStudyRelation()
    {
        return $this->studyRelation;
    }
}
