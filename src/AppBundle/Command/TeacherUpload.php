<?php

namespace AppBundle\Command;


use AppBundle\Services\TranslitService;
use Box\Spout\Reader\ReaderInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use UserBundle\Entity\User;
use UserBundle\Services\User\CreateUser;

/**
 * Class TeacherUpload
 * @package AppBundle\Command
 */
class TeacherUpload extends ContainerAwareCommand
{
    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @var EntityManager
     */
    private $manager;

    /**
     * @var CreateUser
     */
    private $userCreate;

    /**
     * @var TranslitService
     */
    private $converter;

    /**
     * @var array
     */
    protected $userData;

    /**
     * @var array
     */
    protected $problemRows;

    /**
     * TeacherUpload constructor.
     * @param KernelInterface $kernel
     * @param EntityManagerInterface $manager
     * @param CreateUser $userCreate
     * @param TranslitService $converter
     * @param string|null $name
     */
    public function __construct(
        KernelInterface $kernel,
        EntityManagerInterface $manager,
        CreateUser $userCreate,
        TranslitService $converter,
        $name = null
    )
    {
        parent::__construct($name);
        $this->kernel = $kernel;
        $this->manager = $manager;
        $this->userCreate = $userCreate;
        $this->converter = $converter;
    }

    /**
     * @return array
     */
    private function getTableKeys()
    {
        return [
            'discipline',
            'teacher',
            'typeNote',
            'group',
            'department',
            'selfDepartment',
            'email',
            'additional'
        ];
    }

    /**
     * @see http://opensource.box.com/spout/docs/
     *
     *  Use:
     *  php bin/console csp:data:teacher -p /data/tmp/schedule.xlsx
     */
    protected function configure()
    {
        $this
            ->setName('csp:data:teacher')
            ->setDescription('Add data in DB from file. All files read only app/Resources/files/. Use relative path to file')
            ->addOption('path', 'p', InputOption::VALUE_REQUIRED, 'relative path');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     *
     * @see https://phpspreadsheet.readthedocs.io/en/develop/topics/reading-files/
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = sprintf('%s/Resources/files%s', $this->kernel->getRootDir(), $input->getOption('path'));
        $reader = ReaderFactory::create(Type::XLSX); // for XLSX files
        $reader->open($path);
        $data = $this->getDataArray($reader);
        foreach ($data as $item) {
            foreach ($item as $key => $value) {
                try {
                    if ($this->save($value)) {
                        $output->writeln($this->userData['login']);
                        $this->userData = null;
                    }
                } catch (\Exception $e) {
                    $output->writeln(
                        sprintf(
                            '<error>%s%s ROW - %s</error>',
                            $e->getMessage(),
                            PHP_EOL,
                            (string)$key
                        )
                    );
                    $output->writeln(
                        sprintf(
                            '<info>Login - %s</info>',
                            $this->userData['login']
                        )
                    );
                }
            }
        }
        $reader->close();
        $output->writeln('<info>Data adding in DB</info>');

        if (empty($this->problemRows)) {
            return 0;
        }
        $output->writeln('<error>This rows not load in DB:</error>');
        foreach ($this->problemRows as $item) {
            $output->writeln(
                sprintf(' user - %s, problem - %s', $item['login'], $item['problem'])
            );
        }

        return 0;
    }

    /**
     * @param array $data
     * @return bool
     */
    protected function save(array $data)
    {
        $data = array_combine($this->getTableKeys(), $data);
        $data['teacher'] = trim($data['teacher']);
        $teacherNameArray = explode(' ', $data['teacher']);
        $login = $this->converter->convert(sprintf('%s%s', $teacherNameArray[0], $teacherNameArray[1]));
        $loadUser = $this->manager->getRepository(User::class)->findOneBy(['fullname' => $data['teacher']]);
        if (!empty($loadUser)) {
            $data['problem'] = 'User exist';
            $data['login'] = $login;
            $this->problemRows[] = $data;

            return false;
        }
        $user = [
            'login' => $login,
            'password' => sprintf('%s%s%s', $login, rand(0, 9999), 'z'),
            'fullname' => $data['teacher'],
            'active' => true,
            'role' => ['ROLE_TEACHER'],
            'email' => null
        ];
        if (isset($data['email']) && !empty($data['email'])) {
            $user['email'] = $data['email'];
        }
        $this->userData = $user;
        $status = $this->userCreate->createUser($user);

        return $status;
    }

    /**
     * @param ReaderInterface $reader
     * @return array
     */
    protected function getDataArray(ReaderInterface $reader)
    {
        $data = [];
        foreach ($reader->getSheetIterator() as $key => $sheet) {
            $data[$key] = [];
            $keys = null;
            foreach ($sheet->getRowIterator() as $row) {
                if (empty($keys)) {
                    $keys = $row;
                    continue;
                }
                $data[$key][] = $row;
            }
        }

        return $data;
    }
}