<?php

namespace AppBundle\Command;


use AppBundle\Entity\Department;
use Box\Spout\Reader\ReaderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

/**
 * Class DumpData
 * @package AppBundle\Command
 */
class DataUpload extends ContainerAwareCommand
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * DataUpload constructor.
     * @param KernelInterface $kernel
     * @param EntityManagerInterface $manager
     * @param string|null $name
     */
    public function __construct(KernelInterface $kernel, EntityManagerInterface $manager, $name = null)
    {
        parent::__construct($name);
        $this->kernel = $kernel;
        $this->manager = $manager;
    }

    /**
     * @see http://opensource.box.com/spout/docs/
     *
     *  Use:
     *  php bin/console csp:data:upload -p /data/d.xlsx
     */
    protected function configure()
    {
        $this
            ->setName('csp:data:upload')
            ->setDescription('Add data in DB from file. All files read only app/Resources/files/. Use relative path to file')
            ->addOption('path', 'p', InputOption::VALUE_REQUIRED, 'relative path');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     *
     * @see https://phpspreadsheet.readthedocs.io/en/develop/topics/reading-files/
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = sprintf('%s/Resources/files%s', $this->kernel->getRootDir(), $input->getOption('path'));
        $reader = ReaderFactory::create(Type::XLSX); // for XLSX files
        $reader->open($path);
        $data = $this->getDataArray($reader);
        foreach ($data as $item) {
            foreach ($item as $key => $value) {
                try {
                    $this->save($value);
                } catch (\Exception $e) {
                    $output->writeln(sprintf('<error>%s%s ROW - %s</error>', $e->getMessage(), PHP_EOL, (string)$key));
                }
            }
        }
        $reader->close();
        $output->writeln('<info>Data adding in DB</info>');

        return 0;
    }

    /**
     * @param array $data
     */
    protected function save(array $data)
    {
        $department = new Department();
        $department->setName($data['name']);
        $department->setExternalId($data['externalId']);
        $this->manager->persist($department);
        $this->manager->flush();
    }

    /**
     * @param ReaderInterface $reader
     * @return array
     */
    protected function getDataArray(ReaderInterface $reader)
    {
        $data = [];
        foreach ($reader->getSheetIterator() as $key => $sheet) {
            $data[$key] = [];
            $keys = null;
            foreach ($sheet->getRowIterator() as $row) {
                if (empty($keys)) {
                    $keys = $row;
                    continue;
                }
                $data[$key][] = array_combine($keys, $row);
            }
        }

        return $data;
    }
}