<?php

namespace AppBundle\Command;


use AppBundle\Entity\ActiveHistoryStudyRelation;
use AppBundle\Entity\Discipline;
use AppBundle\Entity\NoteType;
use AppBundle\Entity\StudyGroup;
use AppBundle\Entity\StudyRelation;
use Box\Spout\Reader\ReaderInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use UserBundle\Entity\User;
use UserBundle\Services\User\CreateUser;

/**
 * Class TeacherAttach
 * @package AppBundle\Command
 */
class TeacherAttach extends ContainerAwareCommand
{
    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @var EntityManager
     */
    private $manager;

    /**
     * @var CreateUser
     */
    private $userCreate;

    /**
     * @var array
     */
    protected $userData;

    /**
     * @var array
     */
    protected $problemRows;

    /**
     * @var int
     */
    protected $countData = 0;

    /**
     * TeacherUpload constructor.
     * @param KernelInterface $kernel
     * @param EntityManagerInterface $manager
     * @param CreateUser $userCreate
     * @param string|null $name
     */
    public function __construct(
        KernelInterface $kernel,
        EntityManagerInterface $manager,
        CreateUser $userCreate,
        $name = null
    )
    {
        parent::__construct($name);
        $this->kernel = $kernel;
        $this->manager = $manager;
        $this->userCreate = $userCreate;
    }

    /**
     * @return array
     */
    private function getTableKeys()
    {
        return [
            'discipline',
            'teacher',
            'noteType',
            'group',
            'department',
            'selfDepartment',
            'email',
            'additional'
        ];
    }

    /**
     * @see http://opensource.box.com/spout/docs/
     *
     *  Use:
     *  php bin/console csp:data:relations -p /data/tmp/schedule.xlsx
     */
    protected function configure()
    {
        $this
            ->setName('csp:data:relations')
            ->setDescription('Add data in DB from file. All files read only app/Resources/files/tmp. Use relative path to file')
            ->addOption('path', 'p', InputOption::VALUE_REQUIRED, 'relative path');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     *
     * @see https://phpspreadsheet.readthedocs.io/en/develop/topics/reading-files/
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = sprintf('%s/Resources/files%s', $this->kernel->getRootDir(), $input->getOption('path'));
        $reader = ReaderFactory::create(Type::XLSX); // for XLSX files
        $reader->open($path);
        $data = $this->getDataArray($reader);
        foreach ($data as $item) {
            foreach ($item as $key => $value) {
                try {
                    if ($this->makeRelation(array_combine($this->getTableKeys(), $value))) {
                        $this->countData++;
                        $output->writeln($this->userData['login']);
                        $this->userData = null;
                    }
                } catch (\Exception $e) {
                    $output->writeln(
                        sprintf(
                            '<error>%s%s ROW - %s</error>',
                            $e->getMessage(),
                            PHP_EOL,
                            (string)$key
                        )
                    );
                    $output->writeln(
                        sprintf(
                            '<info>Login - %s</info>',
                            $this->userData['login']
                        )
                    );
                }
            }
        }
        $reader->close();
        $output->writeln(sprintf('<info>Data adding in DB. TOTAL ROWS: %s</info>', $this->countData));
        if (empty($this->problemRows)) {
            return 0;
        }
        $output->writeln('<error>This rows not load in DB:</error>');
        foreach ($this->problemRows as $item) {
            $output->writeln(
                sprintf('Discipline - %s, user - %s, problem - %s', $item['discipline'], $item['login'], $item['problem'])
            );
        }

        return 0;
    }

    /**
     * @param array $data
     * @return bool
     */
    protected function makeRelation(array $data)
    {
        /** @var User $teacher */
        $data['teacher'] = trim($data['teacher']);
        $teacher = $this->manager->getRepository(User::class)
            ->findOneBy(['fullname' => $data['teacher']]);
        if (empty($teacher)) {
            return false;
        }
        $data['login'] = $teacher->getLogin();
        $discipline = $this->getDiscipline($data['discipline']);
        if (empty($discipline)) {
            $data['problem'] = 'Not discipline';
            $this->problemRows[] = $data;

            return false;
        }

        return $this->saveRows($data, $discipline, $teacher);
    }

    /**
     * @param array $data
     * @param Discipline $discipline
     * @param User $teacher
     * @return bool
     */
    private function saveRows(array $data, Discipline $discipline, User $teacher)
    {
        $typeNoteData = explode('/', $data['noteType']);
        $studyGroups = $this->getStudyGroups($data['group']);
        foreach ($studyGroups as $group) {
            /** @var StudyGroup $studyGroup */
            $studyGroup = $this->manager->getRepository(StudyGroup::class)
                ->findOneBy(['name' => trim($group)]);
            if (empty($studyGroup)) {
                $data['problem'] = 'Not group';
                $this->problemRows[] = $data;

                continue;
            }
            foreach ($typeNoteData as $item) {
                $noteType = $this->manager->getRepository(NoteType::class)
                    ->getNoteTypeByAbbr(trim($item));
                if (empty($noteType)) {
                    $data['problem'] = sprintf('Not note type - %s', $item);
                    $this->problemRows[] = $data;

                    continue;
                }
                $studyRelation = $this->getStudyRelation($studyGroup, $discipline, $teacher, $noteType);
                if (empty($studyRelation)) {
                    $data['problem'] = 'Study relation has already or problem with save';
                    $this->problemRows[] = $data;

                    continue;
                }
                $this->manager->persist($studyRelation);
                $activeHistory = $this->makeActiveHistory();
                $studyRelation->addActiveHistory($activeHistory);
            }
        }
        try {
            $this->manager->flush();

            return true;
        } catch (\Exception $e) {
            $this->problemRows[] = $data;
            return false;
        }
    }

    /**
     * @param StudyGroup $studyGroup
     * @param Discipline $discipline
     * @param User $teacher
     * @param NoteType $noteType
     * @return StudyRelation|null
     */
    private function getStudyRelation(
        StudyGroup $studyGroup,
        Discipline $discipline,
        User $teacher,
        NoteType $noteType
    )
    {
        $studyRelation = $this->manager->getRepository(StudyRelation::class)
            ->findOneBy([
                'studyGroup' => $studyGroup->getId(),
                'discipline' => $discipline->getId(),
                'teacher' => $teacher->getId(),
                'noteType' => $noteType->getId()
            ]);
        if (!empty($studyRelation)) {
            return null;
        }
        $studyRelation = new StudyRelation();
        $studyRelation->setStudyGroup($studyGroup);
        $studyRelation->setDiscipline($discipline);
        $studyRelation->setTeacher($teacher);
        $studyRelation->setNoteType($noteType);

        return $studyRelation;
    }

    /**
     * @return ActiveHistoryStudyRelation
     */
    private function makeActiveHistory()
    {
        $activeHistory = new ActiveHistoryStudyRelation();
        $activeHistory->setStatus(1);
        $this->manager->persist($activeHistory);

        return $activeHistory;
    }

    /**
     * @param string $discipline
     * @param string $department
     * @return Discipline|null
     */
    private function getDiscipline($discipline)
    {
        $loadDiscipline = $this->manager->getRepository(Discipline::class)
            ->loadDisciplineByFullTitle($discipline);
        if (empty($loadDiscipline)) {
            return null;
        }

        return $loadDiscipline;
    }

    /**
     * @param $groups
     * @return array
     */
    private function getStudyGroups($groups)
    {
        $groups = explode('-', $groups);
        if (count($groups) == 1) {
            return $groups;
        }
        $additionalGroups = [];
        if (($groups[1] - $groups[0]) > 1) {
            for ($i = 0; $i <= ($groups[1] - $groups[0]); $i++) {
                $additionalGroups[] = (int)$groups[0] + $i;
            }
        } else {
            return $groups;
        }

        return $additionalGroups;
    }

    /**
     * @param ReaderInterface $reader
     * @return array
     */
    protected function getDataArray(ReaderInterface $reader)
    {
        $data = [];
        foreach ($reader->getSheetIterator() as $key => $sheet) {
            $data[$key] = [];
            $keys = null;
            foreach ($sheet->getRowIterator() as $row) {
                if (empty($keys)) {
                    $keys = $row;
                    continue;
                }
                $data[$key][] = $row;
            }
        }

        return $data;
    }
}