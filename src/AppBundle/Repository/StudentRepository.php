<?php

namespace AppBundle\Repository;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\QueryBuilder;
use DateTime;

/**
 * StudentRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class StudentRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param $id
     * @return array|null
     */
    public function getStudents($id)
    {
        $query = $this->getEntityManager()->createQueryBuilder()
            ->select('s')
            ->from('AppBundle:Student', 's')
            ->where('s.studyGroup = :id')
            ->orderBy('s.name')
            ->setParameter('id', $id);

        try {
            return $query->getQuery()->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
     * @param $year
     * @return mixed|null
     */
    public function getStudentsByGroups($year)
    {
        $query = $this->getEntityManager()->createQueryBuilder()
            ->select('COUNT(s)')
            ->from('AppBundle:Student', 's')
            ->leftJoin('s.studyGroup', 'sg', 's.studyGroup = sg.id')
            ->where('sg.name LIKE :group_year')
            ->setParameter('group_year', sprintf('%s%%', $year));

        try {
            return $query->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            return null;
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param $condition
     * @return QueryBuilder
     */
    public function createNoteQuery($condition)
    {
        $query = $this->getEntityManager()->createQueryBuilder()
            ->select('s.id')
            ->from('AppBundle:StudentNote', 'sn')
            ->leftJoin('sn.student', 's', 'sn.student = s.notes')
            ->leftJoin('sn.studyRelation', 'sr', 'sr.id = sn.studyRelation')
            ->leftJoin('sr.noteType', 'nt', 'nt.id = sr.noteType')
            ->leftJoin('sr.activeHistory', 'ah', 'sr.id = id = ah.studyRelation')
            ->leftJoin('sr.studyGroup', 'sg', 'sr.studyGroup = sg.id')
            ->leftJoin('sr.discipline', 'd', 'sr.discipline = d.id')
            ->where('sg.name LIKE :group_year AND sn.note > 0 and ah.status = 1')
            ->andWhere('sn.updatedAt IN (SELECT MAX(stn.updatedAt) FROM AppBundle\Entity\StudentNote stn GROUP BY stn.studyRelation, stn.student)')
            ->groupBy('s.id')
            ->having(sprintf('COUNT(s.id) %s', $condition))
            ->orderBy('s.id');

        return $query;
    }

    /**
     * @param QueryBuilder $query
     * @return QueryBuilder
     */
    public function addYearCondition(QueryBuilder $query)
    {
        return $query->andWhere('sg.name LIKE :group_year and ah.status = 1');
    }

    /**
     * @param QueryBuilder $query
     * @param string $condition
     * @return QueryBuilder
     */
    public function addNoteCondition(QueryBuilder $query, $condition = '> 0')
    {
        return $query->andWhere(sprintf('sn.note %s', $condition));
    }

    /**
     * @param QueryBuilder $query
     * @return QueryBuilder
     */
    public function addGroupBy(QueryBuilder $query){
        return $query->groupBy('sr.id');
    }

    /**
     * @param QueryBuilder $query
     * @return QueryBuilder
     */
    public function addTimeCondition(QueryBuilder $query)
    {
        return $query->andWhere(
            '( 
  MONTH(NOW()) >= 9 OR MONTH(NOW()) < 2 
  and sn.createdAt BETWEEN CAST(:begin_date_first_semester AS DATE) and CAST(:end_date_first_semester AS DATE)
)
  OR
(
  MONTH(NOW()) >= 2 and MONTH(NOW()) < 9 
  and sn.createdAt BETWEEN CAST(:begin_date_second_semester AS DATE) and CAST(:end_date_second_semester AS DATE)
)'
        );
    }

    /**
     * @param QueryBuilder $query
     * @return QueryBuilder
     */
    public function addActiveRelationCondition(QueryBuilder $query)
    {
        return $query->andWhere(
            $query->expr()->in(
                'ah.createdAt',
                $this->getEntityManager()->createQueryBuilder()
                    ->select('MAX(ahsr.createdAt) as createdAt')
                    ->from('AppBundle\Entity\ActiveHistoryStudyRelation', 'ahsr')
                    ->groupBy('ahsr.studyRelation')
                    ->getDQL()
            )
        );
    }

    /**
     * @param QueryBuilder $subQuery
     * @return QueryBuilder
     */
    public function getStudentsCredits(QueryBuilder $subQuery)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query = $query->select('COUNT(DISTINCT std.id)')
            ->from('AppBundle:Student', 'std')
            ->where(
                $query->expr()->in('std.id', $subQuery->getDQL())
            );

        return $query;
    }

    /**
     * @param QueryBuilder $query
     * @return QueryBuilder
     */
    public function joinLikeCondition(QueryBuilder $query)
    {
        $query
            ->andWhere('nt.name LIKE :noteType');

        return $query;
    }

    /**
     * @param QueryBuilder $subQuery
     * @return QueryBuilder
     */
    public function addNotLikeCondition(QueryBuilder $subQuery)
    {
        $subQuery
            ->andWhere('nt.name NOT LIKE :noteType');

        return $subQuery;
    }

    /**
     * @param QueryBuilder $query
     * @return mixed|null
     */
    public function runStudentCreditsQuery(QueryBuilder $query)
    {
        try {
            return $query->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            return null;
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param QueryBuilder $query
     * @param array $params
     * @return QueryBuilder
     */
    public function addParams(QueryBuilder $query, array $params)
    {
        if (date("m") < 9) {
            $firstSemesterBeginFactor = -1;
            $semesterFactor = 0;
        } else {
            $firstSemesterBeginFactor = 0;
            $semesterFactor = 1;
        }
        $paramsCollection = new ArrayCollection([
            new Parameter(
                'begin_date_first_semester',
                (new DateTime(sprintf('%s.%s', $params['begin'], date("Y") + $firstSemesterBeginFactor)))->format('Y.m.d')
            ),
            new Parameter(
                'end_date_first_semester',
                (new DateTime(sprintf('%s.%s', $params['end'], date("Y") + $semesterFactor)))->format('Y.m.d')
            ),
            new Parameter(
                'begin_date_second_semester',
                (new DateTime(sprintf('%s.%s', $params['end'], date("Y") + $semesterFactor)))->format('Y.m.d')
            ),
            new Parameter(
                'end_date_second_semester',
                (new DateTime(sprintf('%s.%s', $params['begin'], date("Y") + $semesterFactor)))->format('Y.m.d')
            ),
            new Parameter(
                'group_year',
                sprintf('%s%%', $params['year'])
            )
        ]);
        if (isset($params['noteType']) && !empty($params['noteType'])) {
            $paramsCollection->add(new Parameter(
                'noteType', sprintf('%%%s%%', $params['noteType'])
            ));
        }
        if (isset($params['beginDate']) && !empty($params['beginDate'])) {
            $paramsCollection->add(new Parameter(
                'beginDate', $params['beginDate']
            ));
        }
        if (isset($params['endDate']) && !empty($params['endDate'])) {
            $paramsCollection->add(new Parameter(
                'endDate', $params['endDate']
            ));
        }

        return $query->setParameters($paramsCollection);
    }

    /**
     * @return QueryBuilder
     */
    public function getLessonCredits()
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('sn')
            ->from('AppBundle:StudentNote', 'sn')
            ->leftJoin('sn.student', 's', 'sn.student = s.notes')
            ->leftJoin('sn.studyRelation', 'sr', 'sr.id = sn.studyRelation')
            ->leftJoin('sr.studyGroup', 'sg', 'sr.studyGroup = sg.id')
            ->leftJoin('sr.noteType', 'nt', 'nt.id = sr.noteType')
            ->leftJoin('sr.activeHistory', 'ah', 'sr.id = id = ah.studyRelation');
    }

    /**
     * @param QueryBuilder $query
     * @return mixed|null
     */
    public function runLessonCreditsQuery(QueryBuilder $query)
    {
        $query->orderBy('sn.updatedAt');
        try {
            return $query->getQuery()->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
     * @param QueryBuilder $query
     * @return QueryBuilder
     */
    public function addPeriod(QueryBuilder $query)
    {
        return $query
            ->andWhere('sn.updatedAt BETWEEN :beginDate AND :endDate');
    }
}
