<?php

namespace AppFixtureBundle\DataFixtures\ORM;

use AppBundle\Entity\Specialty;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class SpecialtyDataFixtures
 * @package AppFixtureBundle\DataFixtures\ORM
 */
class SpecialtyDataFixtures extends AbstractFixture implements FixtureInterface, ORMFixtureInterface, OrderedFixtureInterface
{
    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 4;
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 4; $i++) {
            $specialty = new Specialty();
            $specialty->setName(sprintf('specialty %s', $i));
            $specialty->setDepartment($this->getReference(sprintf('department%s', $i + 1)));
            $specialty->setExternalId($i + 1);
            $manager->persist($specialty);
            try {
                $manager->flush();
                $this->addReference(sprintf('specialty%s', $i + 1), $specialty);

            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        }
    }
}