<?php

namespace AppFixtureBundle\DataFixtures\ORM;

use AppBundle\Entity\Student;
use AppBundle\Entity\StudentNote;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

/**
 * Class StudentNoteDataFixtures
 * @package AppFixtureBundle\DataFixtures\ORM
 */
class StudentNoteDataFixtures extends AbstractFixture implements FixtureInterface, ORMFixtureInterface, OrderedFixtureInterface
{
    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 9;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < StudentDataFixtures::STUDENT_COUNT; $i++) {
            $student = $this->getReference(sprintf('student%s', $i + 1));
            for ($j = 0; $j < NoteTypeDataFixtures::getCountNoteType(); $j++) {
                $note = new StudentNote();
                $note->setStudyRelation($this->getStudyRelation($i));
                $note->setStudent($student);
                $note->setNote(rand(0, 30) ? rand(0, 30) : 0);
                $note->setNoteType($this->getReference(sprintf('noteType%s', $j + 1)));
                $manager->persist($note);
            }
        }
        try {
            $manager->flush();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param $i
     * @return object
     */
    private function getStudyRelation($i)
    {
        $relationNumber = ($i - 1) * (StudyRelationDataFixtures::STUDY_RELATION_COUNT  - 1) / (StudentDataFixtures::STUDENT_COUNT- 0) + 1;

        return $this->getReference(sprintf('studyRelation%s', ceil($relationNumber)));
    }
}