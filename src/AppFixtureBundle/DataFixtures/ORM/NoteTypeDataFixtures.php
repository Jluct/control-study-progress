<?php

namespace AppFixtureBundle\DataFixtures\ORM;

use AppBundle\Entity\NoteType;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class NoteTypeDataFixtures
 * @package AppFixtureBundle\DataFixtures\ORM
 */
class NoteTypeDataFixtures extends AbstractFixture implements FixtureInterface, ORMFixtureInterface, OrderedFixtureInterface
{
    private static $noteType = [
        'Lorem ipsum',
        'Aenean commodo',
        'Integer tincidunt',
    ];

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 8;
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < $this->getCountNoteType(); $i++) {
            $noteType = new NoteType();
            $noteType->setName(self::$noteType[$i]);
            $manager->persist($noteType);
            try {
                $manager->flush();
                $this->addReference(sprintf('noteType%s', $i + 1), $noteType);
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    /**
     * @return int
     */
    public static function getCountNoteType()
    {
        return count(self::$noteType);
    }
}