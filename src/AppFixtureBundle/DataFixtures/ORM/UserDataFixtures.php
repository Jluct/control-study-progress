<?php

namespace AppFixtureBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use UserBundle\Services\User\CreateUser;

/**
 * Class UserDataFixtures
 * @package AppFixtureBundle\DataFixtures\ORM
 */
class UserDataFixtures extends Fixture implements ORMFixtureInterface, OrderedFixtureInterface
{
    /**
     * @var CreateUser
     */
    private $createUserService;
    /**
     * @var array
     */
    private $userData = [
        'login' => 'user',
        'fullname' => [
            'name' => 'Ivan',
            'surname' => 'Ivanov'
        ],
        'role'=>['ROLE_TEACHER'],
        'password' => '123456',
        'active' => true,
    ];

    /**
     * UserFixtures constructor.
     * @param CreateUser $createUserService
     */
    public function __construct(CreateUser $createUserService)
    {
        $this->createUserService = $createUserService;
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 5; $i++) {
            $data = [];
            $data = array_merge($this->userData, $data);
            $data['login'] = sprintf('%s%s', $data['login'], $i);
            $data['fullname'] = sprintf(
                '%s%s %s%s',
                $data['fullname']['name'],
                $i,
                $data['fullname']['surname'],
                $i
            );
            $data['email'] = sprintf(
                '%s%s@csp.com',
                implode('', $this->userData['fullname']),
                $i
            );
            $data['externalId'] = $i + 1;

            try {
                $this->createUserService->createUser($data);
                $user = $manager->getRepository('UserBundle\Entity\User')
                    ->findOneBy(['login' => $data['login']]);
                $this->addReference(sprintf('user%s', $i + 1), $user);
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 1;
    }
}