<?php

namespace AppFixtureBundle\DataFixtures\ORM;

use AppBundle\Entity\Department;
use AppBundle\Entity\Student;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class DepartmentDataFixtures
 * @package AppFixtureBundle\DataFixtures\ORM
 */
class StudentDataFixtures extends AbstractFixture implements FixtureInterface, ORMFixtureInterface, OrderedFixtureInterface
{
    const STUDENT_COUNT = 100;

    /**
     * @var array
     */
    private $data = [
        'lorem', 'ipsum', 'dolor', 'sit', 'amet', 'consectetur', 'adipiscing', 'elit',
        'mauris', 'dapibus', 'risus', 'quis', 'suscipit', 'vulputate',
        'eros', 'diam', 'egestas', 'libero', 'eu', 'vulputate', 'risus'
    ];

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 6;
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < self::STUDENT_COUNT; $i++) {
            $name = $this->data[rand(0, count($this->data) - 1)];
            $surname = $this->data[rand(0, count($this->data) - 1)];

            $student = new Student();
            $student->setName(sprintf('%s %s', ucfirst($name), ucfirst($surname)));
            $student->setExternalId($i + 1);
            $student->setStudyGroup($this->getStudyGroup($i));
            $manager->persist($student);
            try {
                $manager->flush();
                $this->addReference(sprintf('student%s', $i + 1), $student);
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    /**
     * @param int $i
     * @return object
     */
    private function getStudyGroup($i)
    {
        $groupNumber = ($i - 1) * (4 - 1) / (99 - 0) + 1;

        return $this->getReference(sprintf('studyGroup%s', ceil($groupNumber)));
    }
}