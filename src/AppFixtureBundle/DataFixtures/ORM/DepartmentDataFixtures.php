<?php

namespace AppFixtureBundle\DataFixtures\ORM;

use AppBundle\Entity\Department;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class DepartmentDataFixtures
 * @package AppFixtureBundle\DataFixtures\ORM
 */
class DepartmentDataFixtures extends AbstractFixture implements FixtureInterface, ORMFixtureInterface, OrderedFixtureInterface
{
    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 2;
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 4; $i++) {
            $department = new Department();
            $department->setName(sprintf('department %s', $i));
            $department->setExternalId($i + 1);
            $manager->persist($department);
            try {
                $manager->flush();
                $this->addReference(sprintf('department%s', $i + 1), $department);

            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        }
    }
}