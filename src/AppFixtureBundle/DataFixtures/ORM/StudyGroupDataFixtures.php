<?php

namespace AppFixtureBundle\DataFixtures\ORM;

use AppBundle\Entity\Department;
use AppBundle\Entity\StudyGroup;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class DepartmentDataFixtures
 * @package AppFixtureBundle\DataFixtures\ORM
 */
class StudyGroupDataFixtures extends AbstractFixture implements FixtureInterface, ORMFixtureInterface, OrderedFixtureInterface
{
    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 5;
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 4; $i++) {
            $studyGroup = new StudyGroup();
            $studyGroup->setName(sprintf('studyGroup %s', $i));
            $studyGroup->setSpecialty($this->getReference(sprintf('specialty%s', $i + 1)));
            $studyGroup->setExternalId('888' . ($i + 1));
            $manager->persist($studyGroup);
            try {
                $manager->flush();
                $this->addReference(sprintf('studyGroup%s', $i + 1), $studyGroup);
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        }
    }
}