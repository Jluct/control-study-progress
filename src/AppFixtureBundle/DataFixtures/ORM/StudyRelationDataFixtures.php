<?php

namespace AppFixtureBundle\DataFixtures\ORM;

use AppBundle\Entity\ActiveHistoryStudyRelation;
use AppBundle\Entity\Department;
use AppBundle\Entity\StudyRelation;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class DepartmentDataFixtures
 * @package AppFixtureBundle\DataFixtures\ORM
 */
class StudyRelationDataFixtures extends AbstractFixture implements FixtureInterface, ORMFixtureInterface, OrderedFixtureInterface
{
    const STUDY_RELATION_COUNT = 4;
    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 7;
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $count = 0;
        for ($i = 0; $i < self::STUDY_RELATION_COUNT; $i++) {
            $studyRelation = new StudyRelation();
            for ($j = 0; $j < 5; $j++) {
                $studyRelation->setStudyGroup($this->getReference(sprintf('studyGroup%s', $i + 1)));
                $studyRelation->setDiscipline($this->getReference(sprintf('discipline%s', 1 + $j + $i)));
                $studyRelation->setTeacher($this->getReference(sprintf('user%s', $i + 1)));
                $studyRelation->setExternalId('888' . $count);
                $activeHistory = new ActiveHistoryStudyRelation();
                $activeHistory->setStatus(true);
                $studyRelation->addActiveHistory($activeHistory);
                $manager->persist($studyRelation);
                $count++;
            }
            try {
                $manager->flush();
                $this->addReference(sprintf('studyRelation%s', $i + 1), $studyRelation);
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        }
    }
}