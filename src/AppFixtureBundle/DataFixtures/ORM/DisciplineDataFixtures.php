<?php

namespace AppFixtureBundle\DataFixtures\ORM;

use AppBundle\Entity\Discipline;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class DisciplineDataFixtures
 * @package AppFixtureBundle\DataFixtures\ORM
 */
class DisciplineDataFixtures extends AbstractFixture implements FixtureInterface, ORMFixtureInterface, OrderedFixtureInterface
{
    const DISCIPLINE_COUNT = 8;

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 3;
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < self::DISCIPLINE_COUNT; $i++) {
            $discipline = new Discipline();
            $discipline->setName(sprintf('discipline %s', $i));
            $discipline->setExternalId($i + 1);
            $manager->persist($discipline);
            try {
                $manager->flush();
                $this->addReference(sprintf('discipline%s', $i + 1), $discipline);
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
        }
    }
}