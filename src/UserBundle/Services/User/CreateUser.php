<?php

namespace UserBundle\Services\User;


use Doctrine\DBAL\Driver\PDOException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use UserBundle\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class CreateUser
 * @package Jluct\BlogBundle\Services\User
 */
class CreateUser
{
    /**
     * @var EntityManager
     */
    private $manager;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var User
     */
    private $user;

    /**
     * @var array
     */
    private $errors = [];

    /**
     * CreateUser constructor.
     * @param EntityManager $manager
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param ValidatorInterface $validator
     */
    public function __construct(EntityManager $manager, UserPasswordEncoderInterface $passwordEncoder, ValidatorInterface $validator)
    {
        $this->manager = $manager;
        $this->encoder = $passwordEncoder;
        $this->validator = $validator;
    }

    /**
     * @return bool
     */
    public function validate()
    {
        if (empty($this->user)) {
            throw new \LogicException();
        }
        $this->errors = $this->validator->validate($this->user);

        return count($this->errors) == 0;
    }

    /**
     * @param array $array
     * @return bool|PDOException|\Exception
     */
    public function createUser(array $array)
    {
        $this->makeEntity($array);
        if (!$this->validate()) {
            return false;
        }
        $this->saveEntity();

        return true;
    }

    /**
     * @param array $array
     */
    public function makeEntity(array $array)
    {
        $this->user = new User();
        $this->user->setLogin($array['login']);
        $this->user->setFullName($array['fullname']);
        $this->user->setPassword($array['password']);
        $password = $this->encoder->encodePassword($this->user, $this->user->getPassword());
        $this->user->setEmail(isset($array['email']) ? $array['email'] : null);
        $this->user->setExternalId(isset($array['externalId']) ? $array['externalId'] : null);
        $this->user->setIsActive(isset($array['active']) ? $array['active'] : false);

        $this->user->setPassword($password);
        if (key_exists('role', $array)) {
            $this->user->setRole($array['role']);
        }
    }

    public function saveEntity()
    {
        if (empty($this->user)) {
            return;
        }
        $this->manager->persist($this->user);
        $this->manager->flush();
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
}