<?php

namespace UserBundle\Entity;

use AppBundle\Entity\GroupNote;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use UserBundle\Form\Model\CreateUserApi;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserRepository")
 * @UniqueEntity("login")
 * @UniqueEntity(
 *  fields={"email"},
 *     message="user.email"
 * )
 */
class User implements UserInterface, \Serializable
{
    /**
     * @var int
     *
     * @Groups({"api_view"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Groups({"api_view"})
     * @ORM\Column(name="login", type="string", unique=true, length=30)
     * @Assert\NotBlank()
     */
    private $login;

    /**
     * @var string
     *
     * @Groups({"api_view"})
     * @ORM\Column(name="fullname", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $fullname;

    /**
     * @var array
     *
     * @ORM\Column(name="role", type="array")
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min=6,
     *     max=64
     * )
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=60, unique=true, nullable=true)
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $salt;

    /**
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    private $isActive;

    /**
     * @var int
     *
     * @Groups({"api_view"})
     * @ORM\Column(name="external_id", type="string", length=32, unique=true, nullable=true)
     */
    private $externalId;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\StudyRelation", mappedBy="teacher")
     */
    private $relations;

    /**
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    private $apiKey;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdApiKey;

    /**
     * @var GroupNote
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\GroupNote", mappedBy="teacher")
     */
    private $notes;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->relations = new ArrayCollection();
        $this->notes = new ArrayCollection();
        $this->isActive = true;
        $this->salt = md5(uniqid(null, true));
        $this->addRole("ROLE_USER");
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->role;
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->login,
            $this->fullname,
            $this->password,
            $this->isActive,
//            $this->salt,
            $this->externalId
        ]);
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->login,
            $this->fullname,
            $this->password,
            $this->externalId,
            $this->isActive,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    /**
     * @param CreateUserApi $user
     */
    public function updateFromApi(CreateUserApi $user)
    {
        $this->setEmail($user->getEmail());
        $this->setFullName($user->getFullname());
        $this->setIsActive($user->isActive());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @return int
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param int $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * @param string $login
     *
     * @return $this
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add role
     *
     * @param string $role
     *
     * @return User
     */
    public function addRole($role)
    {
        $this->role [] = $role;

        return $this;
    }


    /**
     * Set fullname
     *
     * @param string $fullname
     *
     * @return User
     */
    public function setFullName($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullname;
    }

    /**
     * Set role
     *
     * @param array $role
     *
     * @return User
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return array
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Add relation
     *
     * @param \AppBundle\Entity\StudyRelation $relation
     *
     * @return User
     */
    public function addRelation(\AppBundle\Entity\StudyRelation $relation)
    {
        $this->relations[] = $relation;

        return $this;
    }

    /**
     * Remove relation
     *
     * @param \AppBundle\Entity\StudyRelation $relation
     */
    public function removeRelation(\AppBundle\Entity\StudyRelation $relation)
    {
        $this->relations->removeElement($relation);
    }

    /**
     * Get relations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRelations()
    {
        return $this->relations;
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param mixed $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
        $this->setCreatedApiKey(new \DateTime());
    }

    /**
     * @return mixed
     */
    public function getCreatedApiKey()
    {
        return $this->createdApiKey;
    }

    /**
     * @param mixed $createdApiKey
     */
    public function setCreatedApiKey($createdApiKey)
    {
        $this->createdApiKey = $createdApiKey;
    }

    /**
     * Add note
     *
     * @param \AppBundle\Entity\GroupNote $note
     *
     * @return User
     */
    public function addNote(\AppBundle\Entity\GroupNote $note)
    {
        $this->notes[] = $note;

        return $this;
    }

    /**
     * Remove note
     *
     * @param \AppBundle\Entity\GroupNote $note
     */
    public function removeNote(\AppBundle\Entity\GroupNote $note)
    {
        $this->notes->removeElement($note);
    }

    /**
     * Get notes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotes()
    {
        return $this->notes;
    }
}
