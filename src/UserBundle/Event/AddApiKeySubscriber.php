<?php

namespace UserBundle\Event;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use UserBundle\Entity\User;

/**
 * Class AddApiKey
 * @package UserBundle\Event
 */
class AddApiKeySubscriber implements EventSubscriberInterface
{
    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * AddApiKeyListener constructor.
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [KernelEvents::RESPONSE => 'injectApiKey'];
    }

    /**
     * @param FilterResponseEvent $event
     */
    public function injectApiKey(FilterResponseEvent $event)
    {
        $authToken = $this->tokenStorage->getToken();
        if (!empty($authToken) && !$authToken instanceof AnonymousToken) {
            $user = $authToken->getUser();
            if ($user && $user instanceof User && $user->getRole()[0] === 'ROLE_API') {
                $event->getResponse()->headers->set('X-AUTH-TOKEN', $user->getApiKey());
            }
        }
    }
}