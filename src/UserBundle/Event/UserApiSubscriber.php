<?php

namespace UserBundle\Event;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use UserBundle\Services\GenerateTokenService;

/**
 * Class UserApiSubscriber
 * @package UserBundle\Event
 */
class UserApiSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var GenerateTokenService
     */
    protected $generator;

    /**
     * @var AuthorizationChecker
     */
    protected $authChecker;

    /**
     * UserApiSubscriber constructor.
     * @param EntityManagerInterface $entityManager
     * @param GenerateTokenService $generator
     * @param AuthorizationChecker $authChecker
     */
    public function __construct(EntityManagerInterface $entityManager, GenerateTokenService $generator, AuthorizationChecker $authChecker)
    {
        $this->entityManager = $entityManager;
        $this->generator = $generator;
        $this->authChecker = $authChecker;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            AuthenticationEvents::AUTHENTICATION_SUCCESS => 'onAuthenticationUserApi',
        ];
    }

    /**
     * @param AuthenticationEvent $event
     */
    public function onAuthenticationUserApi(AuthenticationEvent $event)
    {
        $authToken = $event->getAuthenticationToken();
        $user = $authToken->getUser();
        if (!$authToken instanceof AnonymousToken) {
            if ($user->getRole()[0] == 'ROLE_API') {
                $user->setApiKey(
                    $this->generator->generateToken(
                        [
                            $user->getUsername(),
                            $user->getSalt(),
                            (new \DateTime())->format('Y-m-d H:i:s')
                        ]
                    )
                );
                $this->entityManager->persist($user);
                $this->entityManager->flush();
            }
        }
    }
}