<?php

namespace UserBundle\Security;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use UserBundle\Services\GenerateTokenService;

/**
 * Class TokenAuthenticator
 * @package UserBundle\Security
 * @see https://symfony.com/doc/3.4/security/guard_authentication.html
 */
class TokenAuthenticator extends AbstractGuardAuthenticator
{
    const VALID_TIME = '60';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @var Response
     */
    private $response;

    /**
     * @var \Twig_Environment
     */
    private $templating;

    /**
     * @var array
     */
    private $errors = [];

    /**
     * @var string
     */
    private $token;

    /**
     * @var GenerateTokenService
     */
    private $generator;

    /**
     * TokenAuthenticator constructor.
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param \Twig_Environment $templating
     * @param GenerateTokenService $generator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder,
        \Twig_Environment $templating,
        GenerateTokenService $generator
    )
    {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->templating = $templating;
        $this->generator = $generator;

        $this->response = new Response();
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function supports(Request $request)
    {
        return $request->headers->has('X-AUTH-TOKEN');
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getCredentials(Request $request)
    {
        return [
            'token' => $request->headers->get('X-AUTH-TOKEN'),
        ];
    }

    /**
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     * @return null|UserInterface
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $userRepository = $this->entityManager->getRepository('UserBundle:User');
        if (!empty($credentials['token'])) {
            return $userRepository->loadUserByApiKey($credentials['token']);
        }
        $this->errors['empty_request'] = 'Request not have token or auth data';
        $this->response->setStatusCode(Response::HTTP_BAD_REQUEST);

        return null;
    }

    /**
     * @param mixed $credentials
     * @param UserInterface $user
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        if (empty($user)) {
            return false;
        }
        $validAuth = false;
        if (!empty($credentials['token'])) {
            $validAuth = $this->checkoutByToken($user);
        }

        return $validAuth;
    }

    /**
     * @param UserInterface $user
     * @return bool
     */
    private function checkoutByToken(UserInterface $user)
    {
        if (empty($user->getApiKey())) {
            $this->errors['empty_token'] = 'Token is empty';
            $this->response->setStatusCode(Response::HTTP_BAD_REQUEST);

            return false;
        }
        $lastUpdate = strtotime((new \DateTime())->format('Y:m:d H:i:s')) - strtotime($user->getCreatedApiKey()->format('Y:m:d H:i:s'));
        if ($lastUpdate > self::VALID_TIME) {
            $this->errors['expired_token'] = 'Token is expired';
            $this->response->setStatusCode(Response::HTTP_BAD_REQUEST);

            return false;
        }

        return true;
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey
     * @return Response
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // on success, let the request continue
        return null;
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     * @return Response
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return $this
            ->response
            ->setContent($this->templating->render('@User/auth/base.xml.twig', ['errors' => $this->errors]));
    }

    /**
     * Called when authentication is needed, but it's not sent
     *
     * @param Request $request
     * @param AuthenticationException|null $authException
     * @return JsonResponse
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = [
            // you might translate this message
            'message' => 'Authentication Required'
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @return bool
     */
    public function supportsRememberMe()
    {
        return false;
    }
}