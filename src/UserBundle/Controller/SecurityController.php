<?php

namespace UserBundle\Controller;


use UserBundle\Entity\User;
use UserBundle\Form\Model\ChangePassword;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Form\UserRegistrationType;

/**
 * Class SecurityController
 * @package UserBundle\Controller
 */
class SecurityController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction()
    {
        $helper = $this->get('security.authentication_utils');

        return $this->render('@User/Security/login.html.twig', [
            'last_username' => $helper->getLastUsername(),
            'error' => $helper->getLastAuthenticationError()
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function checkAction()
    {
        return $this->render('@User/Security/login.html.twig', []);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function changePasswordAction(Request $request)
    {
        $errors = [];
        $changePasswordModel = new ChangePassword();
        $form = $this->createForm('UserBundle\Form\ChangePasswordType', $changePasswordModel);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $passwordEncoder = $this->get('security.password_encoder');
            $user->setPassword(
                $passwordEncoder->encodePassword($user, $form->getNormData()->getNewPassword())
            );
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($user);
            try {
                $manager->flush();
            } catch (\Exception $e) {
                $errors[]['message'] = $this->get('translator')->trans('error_500', [], 'validators');
            }
        }

        return $this->render('@User/Security/change_password.html.twig', [
            'form' => $form->createView(),
            'errors' => $errors
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function registrationAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserRegistrationType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $this->container
                ->get('security.password_encoder')
                ->encodePassword($user, $form->getNormData()->getPassword());
            $user->setPassword($password);
            $user->setRole(['ROLE_TEACHER']);
            $this->getDoctrine()->getManager()->persist($user);
            $administrations = $this->getDoctrine()->getRepository('UserBundle:User')->getAdministrations();
            try {
                $this->getDoctrine()->getManager()->flush();
                $mailer = $this->container->get('mailer');
                $message = (new \Swift_Message($this->container->get('translator')->trans('registration.congratulation', [], 'UserBundle')))
                    ->setFrom($this->getParameter('mailer_user'))
                    ->setTo($user->getEmail())
                    ->setBody(
                        $this->renderView(
                            '@User/Security/mail/registration.html.twig',
                            [
                                'user' => $user,
                                'password' => $request->get('userbundle_user')['password']['first'],
                                'administrations' => $administrations,
                                'host' => $request->getHttpHost()
                            ]
                        ),
                        'text/html'
                    );

                $mailer->send($message);
                $this->addFlash(
                    'success',
                    $this->container->get('translator')->trans('registration.congratulation', [], 'UserBundle')
                );

                return $this->redirect($this->generateUrl('user_registration_confirmed'));
            } catch (\Exception $e) {
                $this->addFlash(
                    'danger',
                    $this->container->get('translator')->trans('registration.failed', [], 'UserBundle')
                );
            }
        }

        return $this->render('@User/Security/registration.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function registrationConfirmedAction()
    {
        return $this->render('@User/Security/registration-confirmed.html.twig', []);
    }
}
