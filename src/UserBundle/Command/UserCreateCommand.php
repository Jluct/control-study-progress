<?php

namespace UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class JbUserCreateAdminCommand
 * @package UserBundle\Command
 */
class UserCreateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('user:create')
            ->setDescription('Creates a user with administrator role')
            ->addOption('login', 'l', InputOption::VALUE_REQUIRED, 'login')
            ->addOption('fullname', 'f', InputOption::VALUE_REQUIRED, 'Full name')
            ->addOption('password', 'p', InputOption::VALUE_REQUIRED, 'Password')
            ->addOption('email', 'm', InputOption::VALUE_REQUIRED, 'Email')
            ->addOption('active', 'a', InputOption::VALUE_OPTIONAL, 'Active user', true)
            ->addOption('role', 'r', InputOption::VALUE_OPTIONAL, 'Role user', 'user');
    }

    /**
     * Created user
     * Example:
     * php bin/console ...
     * user:create -l admin -f administrator -p 123456 -m admin1@mail.ru -a true -r admin
     * OR
     * user:create --login=admin --fullname=admin --password=123456 --email=admin@mail.ru --active=true --role=admin
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $service = $this->getContainer()->get('user.create');
        $data = [];

        $data['login'] = $input->getOption('login');
        $data['fullname'] = $input->getOption('fullname');
        $data['password'] = $input->getOption('password');
        $data['email'] = $input->getOption('email');
        $data['active'] = $input->getOption('active') ? 1 : 0;
        $data['role'] = [$this->getRoles()[$input->getOption('role')]];
        $service->createUser($data);

        $output->writeln('<info>User created</info>');

        return 0;
    }

    /**
     * @return array
     */
    protected function getRoles()
    {
        return [
            'api' => 'ROLE_API',
            'user' => 'ROLE_USER',
            'teacher' => 'ROLE_TEACHER',
            'moder' => 'ROLE_MODER',
            'admin' => 'ROLE_ADMIN'
        ];
    }
}
