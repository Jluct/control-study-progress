<?php

namespace UserBundle\Form\Model;

use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ChangePassword
 * @package UserBundle\Form\Model
 */
class ChangePassword
{
    /**
     * @Assert\NotBlank(
     *     message = "change_password.not_blank"
     * )
     * @SecurityAssert\UserPassword(
     *     message = "change_password.no_valid_password"
     * )
     */
    protected $oldPassword;

    /**
     * @Assert\NotBlank(
     *     message = "change_password.not_blank"
     * )
     * @Assert\Length(
     *     min = 6,
     *     max = 24,
     *     minMessage = "change_password.small_password",
     *     maxMessage = "change_password.long_password"
     * )
     */
    protected $newPassword;

    /**
     * @return mixed
     */
    public function getOldPassword()
    {
        return $this->oldPassword;
    }

    /**
     * @param mixed $oldPassword
     */
    public function setOldPassword($oldPassword)
    {
        $this->oldPassword = $oldPassword;
    }

    /**
     * @return mixed
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }

    /**
     * @param mixed $newPassword
     */
    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;
    }
}