<?php

namespace UserBundle\Form\Model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class CreateUserApi
 * @package UserBundle\Form\Model
 */
class CreateUserApi
{
    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message = "change_password.not_blank"
     * )
     * @Assert\Length(min="3")
     */
    protected $fullname;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\Email(
     *     message = "user.email_no_valid"
     * )
     */
    protected $email;

    /**
     * @var int
     *
     * @Assert\NotBlank(
     *     message = "change_password.not_blank"
     * )
     */
    protected $externalId;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $active;

    /**
     * CreateUserApi constructor.
     * @param array $array
     */
    public function __construct(array $array = [])
    {
        if (empty($array)) {
            return;
        }
        $this->fullname = $array['fullname'];
        $this->externalId = $array['externalId'];
        if (isset($array['email'])) {
            $this->email = $array['email'];
        }
        if (isset($array['active'])) {
            $this->active = $array['active'];
        }
    }

    /**
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * @param string $fullname
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }
}