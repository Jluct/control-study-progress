<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ChangePasswordType
 * @package UserBundle\Form
 */
class ChangePasswordType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('oldPassword', PasswordType::class, [
                'translation_domain' => 'validators',
                'label' => 'password',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('newPassword', RepeatedType::class, [
                'translation_domain' => 'validators',
                'type' => PasswordType::class,
                'invalid_message' => 'change_password.password_must_match',
                'required' => true,
                'first_options' => [
                    'label' => 'new_password',
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ],
                'second_options' => [
                    'label' => 'repeat_new_password',
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ]
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Acme\UserBundle\Form\Model\ChangePassword',
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'change_passwd';
    }
}