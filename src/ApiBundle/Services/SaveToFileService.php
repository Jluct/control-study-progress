<?php

namespace ApiBundle\Services;


use Symfony\Component\Filesystem\Filesystem;

/**
 * Class SaveToFileService
 * @package ApiBundle\Services
 */
class SaveToFileService
{
    /**
     * @var string
     */
    private $dirName = 'DIR';

    /**
     * @var string
     */
    private $fileName = 'file';

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $workDir;

    /**
     * SaveToFileService constructor.
     * @param Filesystem $filesystem
     * @param null $dirName
     * @param null $fileName
     */
    public function __construct(Filesystem $filesystem, $dirName = null, $fileName = null)
    {
        $this->filesystem = $filesystem;
        if ($dirName) {
            $this->dirName = $dirName;
        }
        if ($fileName) {
            $this->fileName = $fileName;
        }
    }

    /**
     * @param string $path
     */
    public function clear($path)
    {
        $this->filesystem->remove(sprintf('%s%s/', $path, $this->dirName));
    }

    /**
     * @param string $path
     * @param string|array $data
     */
    public function write($path, $data)
    {
        $this->workDir = sprintf('%s%s/', $path, $this->dirName);
        if (!$this->filesystem->exists($this->workDir)) {
            $this->filesystem->mkdir($this->workDir);
        }

        $currentFile = $this->getCurrentFile($this->workDir);


        $this->writeData($currentFile, $data);
    }

    /**
     * @param string $file
     * @param string|array $data
     */
    private function writeData($file, $data)
    {
        if (is_array($data)) {
            $dataToString = '';
            foreach ($data as $key => $item) {
                $dataToString = sprintf("%s\n%s - %s ", $dataToString, $key, $item);
            }
            $data = $dataToString;
        }
        $data = sprintf('%s%s', trim($data), PHP_EOL);
        $this->filesystem->appendToFile($file, $data);
    }

    /**
     * @param $dir
     * @param int $maxSize
     * @return string
     */
    private function getCurrentFile($dir, $maxSize = 102400)
    {
        $files = array_slice(scandir($dir), 2);
        $count = 0;
        if (!empty($files)) {
            $count = count($files);
        }

        $currentFile = isset($files[$count - 1]) ? sprintf('%s%s', $dir, $files[$count - 1]) : null;
        if (!file_exists($currentFile) || filesize($currentFile) > $maxSize) {
            $currentFile = sprintf('%s%s-%s.txt', $dir, $this->fileName, $count);
        }

        return $currentFile;
    }
}