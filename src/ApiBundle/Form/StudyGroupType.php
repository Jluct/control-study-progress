<?php

namespace ApiBundle\Form;

use ApiBundle\Form\DataTransformer\TransformEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class StudyGroupType
 * @package ApiBundle\Form
 */
class StudyGroupType extends AbstractType
{
    private $transformer;

    /**
     * StudyGroupType constructor.
     * @param TransformEntity $transformer
     */
    public function __construct(TransformEntity $transformer)
    {
        $transformer->setEntityName('AppBundle:Specialty');
        $this->transformer = $transformer;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('externalId')
            ->add('isActive')
            ->add('specialty', TextType::class, []);

        $builder->get('specialty')
            ->addModelTransformer($this->transformer);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\StudyGroup',
            'csrf_protection' => false
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_studygroup';
    }
}
