<?php

namespace ApiBundle\Form;

use ApiBundle\Form\DataTransformer\TransformEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class StudentType
 * @package ApiBundle\Form
 */
class StudentType extends AbstractType
{
    /**
     * @var TransformEntity
     */
    private $transformer;

    /**
     * StudyGroupType constructor.
     * @param TransformEntity $transformer
     */
    public function __construct(TransformEntity $transformer)
    {
        $transformer->setEntityName('AppBundle:StudyGroup');
        $this->transformer = $transformer;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('externalId')
            ->add('studyGroup', TextType::class, []);

        $builder->get('studyGroup')
            ->addModelTransformer($this->transformer);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Student',
            'csrf_protection' => false
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_student';
    }
}