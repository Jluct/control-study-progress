<?php

namespace ApiBundle\Form;

use ApiBundle\Form\DataTransformer\TransformEntity;
use Doctrine\DBAL\Types\BooleanType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Class StudyRelationType
 * @package ApiBundle\Form
 */
class StudyRelationType extends AbstractType
{
    /**
     * @var TransformEntity
     */
    private $transformer;

    /**
     * StudyGroupType constructor.
     * @param TransformEntity $transformer
     */
    public function __construct(TransformEntity $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('externalId');
        $this->transformer->setEntityName('UserBundle:User');
        $builder
            ->add('teacher', TextType::class, []);
        $builder->get('teacher')
            ->addModelTransformer(clone $this->transformer);

        $this->transformer->setEntityName('AppBundle:Discipline');
        $builder
            ->add('discipline', TextType::class, []);
        $builder->get('discipline')
            ->addModelTransformer(clone $this->transformer);

        $this->transformer->setEntityName('AppBundle:StudyGroup');
        $builder
            ->add('studyGroup', TextType::class, []);
        $builder->get('studyGroup')
            ->addModelTransformer(clone $this->transformer);

        $builder
            ->add('active', BooleanType::class, [

            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\StudyRelation',
            'csrf_protection' => false
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_studyrelation';
    }
}