<?php

namespace ApiBundle\Form\DataTransformer;


use Symfony\Component\Form\DataTransformerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TransformEntity
 * @package ApiBundle\Form\DataTransform
 */
class TransformEntity implements DataTransformerInterface
{
    /**
     * @var string
     */
    private $entityName;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * TransformEntity constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $data
     * @return null|int
     */
    public function transform($data)
    {
        if (empty($data) || !is_object($data)) {
            return null;
        }

        return $data->getExternalId();
    }

    /**
     * @param mixed $value
     * @return null|object
     * @throws \Exception
     */
    public function reverseTransform($value)
    {
        if (empty($this->entityName)) {
            throw new \Exception('Not set property entityName', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        if (!$value) {
            return null;
        }

        return $this->entityManager
            ->getRepository($this->entityName)
            ->findOneBy(['externalId' => $value]);
    }

    /**
     * @return mixed
     */
    public function getEntityName()
    {
        return $this->entityName;
    }

    /**
     * @param string $entityName
     */
    public function setEntityName($entityName)
    {
        $this->entityName = $entityName;
    }
}