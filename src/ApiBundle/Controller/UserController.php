<?php

namespace ApiBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Form\Model\CreateUserApi;

/**
 * Class UserController
 * @package ApiBundle\Controller
 */
class UserController extends DefaultController
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTagName('user');
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function readAction(Request $request)
    {
        $users = $this->get('segment_loader_service')
            ->segment('UserBundle:User', $request->get('page'))
            ->where("d.role LIKE '%ROLE_TEACHER%'")
            ->load();
        if (empty($users)) {
            $this->response->setStatusCode(Response::HTTP_NOT_FOUND);
            $this->dataResponse['errors'][] = 'Users not found';
            $this->dataResponse['status'] = 0;
        } else {
            $this->dataResponse['data'] = $users;
        }

        return $this->response->setContent(
            $this->renderView('@Api/view.xml.twig', $this->dataResponse)
        );
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function createAction(Request $request)
    {
        $user = $request->request->all();
        $userApi = new CreateUserApi();
        $form = $this->createForm('UserBundle\Form\UserApiType', $userApi);
        $form->submit($user);
        if (!$form->isValid()) {
            $this->addValidationError((string)$form->getErrors(true, false), Response::HTTP_BAD_REQUEST);

            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }

        $md5ArrayExternalId = preg_split('//', md5($userApi->getExternalId()), -1, PREG_SPLIT_NO_EMPTY);
        $nameArray = explode(' ', $userApi->getFullname());
        $surname = $nameArray[0];
        $login = strtolower(sprintf('%s%s', $surname, implode('', array_slice($md5ArrayExternalId, -1, -5))));

        $user['role'] = ['ROLE_TEACHER'];
        $user['login'] = $this->get('transliteration_service')->transliteration($login);
        $user['active'] = $userApi->isActive();
        $user['password'] = implode('', array_slice($md5ArrayExternalId, 0, 6));

        $userService = $this->container->get('user.create');
        $userService->makeEntity($user);

        if (!$userService->validate()) {
            $this->addValidationError((string)$userService->getErrors(), Response::HTTP_BAD_REQUEST);

            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }

        try {
            $userService->saveEntity();
            $this->get('save_to_file.service')->write(
                sprintf('%s%s', $this->get('kernel')->getRootDir(), '/Resources/files/'),
                [
                    'date' => (new \DateTime())->format('d-m-Y H:i:s'),
                    'login' => $user['login'],
                    'password' => $user['password']
                ]
            );

        } catch (\Exception $e) {
            $this->addError($e, $user['externalId'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->response->setContent($this->renderView('@Api/create.xml.twig', $this->dataResponse));
    }

    /**
     * @param $e_id
     * @param Request $request
     * @return Response
     */
    public function updateAction($e_id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $user = $em
            ->getRepository('UserBundle:User')
            ->findOneBy(['externalId' => $e_id]);

        if (empty($user)) {
            $this->notFound($e_id);

            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }
        $userApi = new CreateUserApi();
        $form = $this->createForm('UserBundle\Form\UserApiType', $userApi);
        $form->submit($data, false);
        if (!$form->isValid()) {
            $this->addValidationError((string)$form->getErrors(true, false));

            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }

        $user->updateFromApi($userApi);

        try {
            $em->flush();
        } catch (\Exception $e) {
            $this->addError($e, $e_id, Response::HTTP_BAD_REQUEST);
        }

        return $this->response->setContent(
            $this->renderView('@Api/create.xml.twig', $this->dataResponse)
        );
    }

    /**
     * @param $e_id
     * @return Response
     */
    public function deleteAction($e_id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em
            ->getRepository('UserBundle:User')
            ->findOneBy(['externalId' => $e_id]);

        if (empty($user)) {
            $this->notFound($e_id);

            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }

        try {
            $em->remove($user);
            $em->flush();
        } catch (\Exception $e) {
            $this->addError($e, $e_id);
        }

        return $this->response->setContent(
            $this->renderView('@Api/create.xml.twig', $this->dataResponse)
        );
    }

    /**
     * @param $e_id
     * @return Response
     */
    public function existAction($e_id)
    {
        $this->dataResponse['data'] = '1';
        $em = $this->getDoctrine()->getManager();
        $user = $em
            ->getRepository('UserBundle:User')
            ->findOneBy(['externalId' => $e_id]);
        if (empty($user)) {
            $this->dataResponse['data'] = '0';
        }

        return $this->response->setContent(
            $this->renderView('@Api/exist.xml.twig', $this->dataResponse)
        );
    }
}