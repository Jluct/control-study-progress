<?php

namespace ApiBundle\Controller;

use ApiBundle\Form\StudentType;
use AppBundle\Entity\Student;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class StudentController
 * @package ApiBundle\Controller
 */
class StudentController extends DefaultController
{
    /**
     * StudentController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTagName('student');
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function readAction(Request $request)
    {
        $students = $this->get('segment_loader_service')
            ->segment('AppBundle:Student', $request->get('page'))
            ->load();
        if (empty($students)) {
            $this->response->setStatusCode(Response::HTTP_NOT_FOUND);
            $this->dataResponse['errors'][] = 'Page not found';
            $this->dataResponse['status'] = 0;
        } else {
            $this->dataResponse['data'] = $students;
        }

        return $this->response->setContent(
            $this->renderView('@Api/view.xml.twig', $this->dataResponse)
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();

        $student = new Student();
        $form = $this->createForm(StudentType::class, $student);
        $form->submit($data);
        if (!$form->isValid()) {
            $this->addValidationError((string)$form->getErrors(true, false));

            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }

        try {
            $em->persist($student);
            $em->flush();
        } catch (\Exception $e) {
            $this->addError($e, $data['externalId']);
        }

        return $this->response = $this->response->setContent(
            $this->renderView('@Api/create.xml.twig', $this->dataResponse)
        );
    }

    /**
     * @param $e_id
     * @param Request $request
     * @return Response
     */
    public function updateAction($e_id, Request $request)
    {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository('AppBundle:Student')->findOneBy(['externalId' => $e_id]);
        if (empty($student)) {
            $this->notFound($e_id);

            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }
        $form = $this->createForm(StudentType::class, $student);
        $form->submit($data, false);
        if (!$this->validate($student)) {
            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }

        try {
            $em->persist($student);
            $em->flush();
        } catch (\Exception $e) {
            $this->addError($e, $data['externalId']);
        }

        return $this->response = $this->response->setContent(
            $this->renderView('@Api/create.xml.twig', $this->dataResponse)
        );
    }

    /**
     * @param $e_id
     * @return Response
     */
    public function deleteAction($e_id)
    {
        $em = $this->getDoctrine()->getManager();
        $student = $em
            ->getRepository('AppBundle:Student')
            ->findOneBy(['externalId' => $e_id]);

        if (empty($student)) {
            $this->notFound($e_id);

            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }

        try {
            $em->remove($student);
            $em->flush();
        } catch (\Exception $e) {
            $this->addError($e, $e_id);
        }

        return $this->response->setContent(
            $this->renderView('@Api/create.xml.twig', $this->dataResponse)
        );
    }


    /**
     * @param $e_id
     * @return Response
     */
    public function existAction($e_id)
    {
        $this->dataResponse['data'] = '1';
        $em = $this->getDoctrine()->getManager();
        $user = $em
            ->getRepository('AppBundle:Student')
            ->findOneBy(['externalId' => $e_id]);
        if (empty($user)) {
            $this->dataResponse['data'] = '0';
        }

        return $this->response->setContent(
            $this->renderView('@Api/exist.xml.twig', $this->dataResponse)
        );
    }
}
