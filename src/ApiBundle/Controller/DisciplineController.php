<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Discipline;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DisciplineController
 * @package ApiBundle\Controller
 */
class DisciplineController extends DefaultController
{
    /**
     * DisciplineController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTagName('discipline');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function readAction(Request $request)
    {
        $disciplines = $this->get('segment_loader_service')->segment('AppBundle:Discipline', $request->get('page'))->load();
        if (empty($disciplines)) {
            $this->response->setStatusCode(404);
            $this->dataResponse['errors'][] = 'Page not found';
            $this->dataResponse['status'] = 0;
        } else {
            $this->dataResponse['data'] = $disciplines;
        }
        return $this->response->setContent(
            $this->renderView('@Api/view.xml.twig', $this->dataResponse)
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $discipline = new Discipline();
        $discipline->setName(isset($data['name']) ? $data['name'] : '');
        $discipline->setExternalId(isset($data['externalId']) ? $data['externalId'] : '');
        $discipline->setIsActive(isset($data['active']) ? $data['active'] : '');

        if (!$this->validate($discipline)) {
            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }

        try {
            $em->persist($discipline);
            $em->flush();
        } catch (\Exception $e) {
            $this->addError($e, $data['externalId']);
        }

        return $this->response->setContent(
            $this->renderView('@Api/create.xml.twig', $this->dataResponse)
        );
    }

    /**
     * @param $e_id
     * @param Request $request
     * @return Response
     */
    public function updateAction($e_id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $discipline = $em
            ->getRepository('AppBundle:Discipline')
            ->findOneBy(['externalId' => $e_id]);

        if (empty($discipline)) {
            $this->notFound($e_id);

            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }

        $discipline->setName(isset($data['name']) ? $data['name'] : $discipline->getName());
        $discipline->setExternalId(isset($data['externalId']) ? $data['externalId'] : $discipline->getExternalId());
        $discipline->setIsActive(isset($data['active']) ? $data['active'] : $discipline->isActive());

        if (!$this->validate($discipline)) {
            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }

        try {
            $em->flush();
        } catch (\Exception $e) {
            $this->addError($e, $e_id);
        }

        return $this->response->setContent(
            $this->renderView('@Api/create.xml.twig', $this->dataResponse)
        );
    }

    /**
     * @param $e_id
     * @return Response
     */
    public function deleteAction($e_id)
    {
        $em = $this->getDoctrine()->getManager();
        $discipline = $em
            ->getRepository('AppBundle:Discipline')
            ->findOneBy(['externalId' => $e_id]);

        if (empty($discipline)) {
            $this->notFound($e_id);

            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }

        try {
            $em->remove($discipline);
            $em->flush();
        } catch (\Exception $e) {
            $this->addError($e, $e_id);
        }

        return $this->response->setContent(
            $this->renderView('@Api/create.xml.twig', $this->dataResponse)
        );
    }

    /**
     * @param $e_id
     * @return Response
     */
    public function existAction($e_id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em
            ->getRepository('AppBundle:Discipline')
            ->findOneBy(['externalId' => $e_id]);
        if (empty($user)) {
            $this->dataResponse['data'] = '0';
        } else {
            $this->dataResponse['data'] = '1';
        }

        return $this->response->setContent(
            $this->renderView('@Api/exist.xml.twig', $this->dataResponse)
        );
    }
}
