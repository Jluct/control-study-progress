<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Specialty;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SpecialtyController
 * @package ApiBundle\Controller
 */
class SpecialtyController extends DefaultController
{
    /**
     * SpecialtyController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTagName('specialty');
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function readAction(Request $request)
    {
        $specialty = $this->get('segment_loader_service')
            ->segment('AppBundle:Specialty', $request->get('page'))
            ->load();
        if (empty($specialty)) {
            $this->response->setStatusCode(404);
            $this->dataResponse['errors'][] = 'Page not found';
            $this->dataResponse['status'] = 0;
        } else {
            $this->dataResponse['data'] = $specialty;
        }

        return $this->response->setContent(
            $this->renderView('@Api/view.xml.twig', $this->dataResponse)
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $specialty = new Specialty();
        $specialty->setName(isset($data['name']) ? $data['name'] : '');
        $specialty->setExternalId(isset($data['externalId']) ? $data['externalId'] : '');
        $specialty->setIsActive(isset($data['active']) ? $data['active'] : '');

        if (!$this->validate($specialty)) {
            return $this->response = $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }

        try {
            $em->persist($specialty);
            $em->flush();
        } catch (\Exception $e) {
            $this->addError($e, $data['externalId']);
        }

        return $this->response = $this->response->setContent(
            $this->renderView('@Api/create.xml.twig', $this->dataResponse)
        );
    }

    /**
     * @param $e_id
     * @param Request $request
     * @return Response
     */
    public function updateAction($e_id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $specialty = $em
            ->getRepository('AppBundle:Specialty')
            ->findOneBy(['externalId' => $e_id]);

        if (empty($specialty)) {
            $this->notFound($e_id);

            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }

        $specialty->setName(isset($data['name']) ? $data['name'] : $specialty->getName());
        $specialty->setExternalId(isset($data['externalId']) ? $data['externalId'] : $specialty->getExternalId());
        $specialty->setIsActive(isset($data['active']) ? $data['active'] : $specialty->isActive());

        if (!$this->validate($specialty)) {
            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }

        try {
            $em->flush();
        } catch (\Exception $e) {
            $this->addError($e, $e_id);
        }

        return $this->response->setContent(
            $this->renderView('@Api/create.xml.twig', $this->dataResponse)
        );
    }

    /**
     * @param $e_id
     * @return Response
     */
    public function deleteAction($e_id)
    {
        $em = $this->getDoctrine()->getManager();
        $specialty = $em
            ->getRepository('AppBundle:Specialty')
            ->findOneBy(['externalId' => $e_id]);

        if (empty($specialty)) {
            $this->notFound($e_id);

            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }

        try {
            $em->remove($specialty);
            $em->flush();
        } catch (\Exception $e) {
            $this->addError($e, $e_id);
        }

        return $this->response->setContent(
            $this->renderView('@Api/create.xml.twig', $this->dataResponse)
        );
    }

    /**
     * @param $e_id
     * @return Response
     */
    public function existAction($e_id)
    {
        $this->dataResponse['data'] = '1';
        $em = $this->getDoctrine()->getManager();
        $user = $em
            ->getRepository('AppBundle:Specialty')
            ->findOneBy(['externalId' => $e_id]);
        if (empty($user)) {
            $this->dataResponse['data'] = '0';
        }

        return $this->response->setContent(
            $this->renderView('@Api/exist.xml.twig', $this->dataResponse)
        );
    }
}
