<?php

namespace ApiBundle\Controller;


use ApiBundle\Form\StudyRelationType;
use AppBundle\Entity\ActiveHistoryStudyRelation;
use AppBundle\Entity\StudyRelation;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class StudyRelationController
 * @package ApiBundle\Controller
 */
class StudyRelationController extends DefaultController
{
    /**
     * StudentController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTagName('study_relation');
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function readAction(Request $request)
    {
        $relations = $this->get('segment_loader_service')
            ->activeHistory(
                'AppBundle\Entity\StudyRelation',
                'AppBundle\Entity\ActiveHistoryStudyRelation',
                'studyRelation',
                $request->get('page')
            )
            ->load();
        if (empty($relations)) {
            $this->response->setStatusCode(Response::HTTP_NOT_FOUND);
            $this->dataResponse['errors'][] = 'Page not found';
            $this->dataResponse['status'] = 0;
        } else {
            $this->dataResponse['data'] = $relations;
        }

        return $this->response->setContent(
            $this->renderView('@Api/view.xml.twig', $this->dataResponse)
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();

        $studyRelation = new StudyRelation();
        $form = $this->createForm(StudyRelationType::class, $studyRelation);
        $form->submit($data);
        if (!$form->isValid()) {
            $this->addValidationError((string)$form->getErrors(true, false));

            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }
        $this->addActiveHistory($data['active'], $studyRelation, new ActiveHistoryStudyRelation());
        try {
            $em->persist($studyRelation);
            $em->flush();
        } catch (\Exception $e) {
            $this->addError($e, $data['externalId']);
        }

        return $this->response = $this->response->setContent(
            $this->renderView('@Api/create.xml.twig', $this->dataResponse)
        );
    }

    /**
     * @param $e_id
     * @param Request $request
     * @return Response
     */
    public function updateAction($e_id, Request $request)
    {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $studyRelation = $em->getRepository('AppBundle:StudyRelation')->findOneBy(['externalId' => $e_id]);
        if (empty($studyRelation)) {
            $this->notFound($e_id);

            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }

        $form = $this->createForm(StudyRelationType::class, $studyRelation);
        $form->submit($data, false);
        if (!$this->validate($studyRelation)) {
            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }
        if (isset($data['active'])) {
            $this->addActiveHistory($data['active'], $studyRelation, new ActiveHistoryStudyRelation());
        }
        $em->persist($studyRelation);
        try {
            $em->flush();
        } catch (\Exception $e) {
            $this->addError($e, $data['externalId']);
        }

        return $this->response = $this->response->setContent(
            $this->renderView('@Api/create.xml.twig', $this->dataResponse)
        );
    }

    /**
     * @param $e_id
     * @return Response
     */
    public function deleteAction($e_id)
    {
        $em = $this->getDoctrine()->getManager();
        $studyRelation = $em
            ->getRepository('AppBundle:StudyRelation')
            ->findOneBy(['externalId' => $e_id]);

        if (empty($studyRelation)) {
            $this->notFound($e_id);

            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }

        try {
            $em->remove($studyRelation);
            $em->flush();
        } catch (\Exception $e) {
            $this->addError($e, $e_id);
        }

        return $this->response->setContent(
            $this->renderView('@Api/create.xml.twig', $this->dataResponse)
        );
    }
}
