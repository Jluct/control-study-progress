<?php

namespace ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DepartmentController
 * @package ApiBundle\Controller
 */
class AuthController extends DefaultController
{
    /**
     * DisciplineController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTagName('auth');
    }

    /**
     * @return Response
     */
    public function successAuthAction()
    {
        $this->dataResponse['data'] = ['Auth success'];

        return $this->response->setContent(
            $this->renderView('@Api/view.xml.twig', $this->dataResponse)
        );
    }

}
