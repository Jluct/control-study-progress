<?php

namespace ApiBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\EntityInterface\ActiveHistoryInterface;
use AppBundle\Entity\EntityInterface\LeadingActivityHistory;

/**
 * Class DefaultController
 * @package ApiBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @var array
     */
    protected $dataResponse = [
        'errors' => [],
        'status' => true,
        'tagName' => 'entity'
    ];

    /**
     * @var Response
     */
    protected $response;

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->response = new Response();
        $this->response->headers->set('Content-Type', 'text/xml');
    }

    /**
     * @return string
     */
    public function getTagName()
    {
        return $this->dataResponse['tagName'];
    }

    /**
     * @param string $tagName
     */
    public function setTagName($tagName)
    {
        $this->dataResponse['tagName'] = $tagName;
    }

    /**
     * @param $entity
     * @return bool
     */
    protected function validate($entity)
    {
        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if (count($errors) > 0) {
            $this->response->setStatusCode(Response::HTTP_BAD_REQUEST);
            $this->dataResponse['errors'][] = ['valid' => 'No valid data'];
            $this->dataResponse['internal']['errors'] = $errors;
            $this->dataResponse['status'] = 0;

            return false;
        }

        return true;
    }

    /**
     * @param \Exception $error
     * @param int $externalId
     * @param int $httpState
     */
    protected function addError(\Exception $error, $externalId, $httpState = 500)
    {
        $this->response->setStatusCode($httpState);
        $error = [
            'code' => $error->getCode(),
            'message' => $error->getMessage(),
            'externalId' => $externalId
        ];
        $this->dataResponse['internal']['errors'][] = $error;
        $this->dataResponse['status'] = 0;
    }

    /**
     * @param string $errors
     * @param int $httpState
     */
    protected function addValidationError($errors, $httpState = 400)
    {
        $this->response->setStatusCode($httpState);
        if ($httpState == Response::HTTP_NOT_FOUND) {
            $this->dataResponse['errors'][] = ['not_found' => 'Data not found'];
        } else {
            $this->dataResponse['errors'][] = ['bad_request' => 'Data not valid'];
        }

        $this->dataResponse['status'] = 0;
        $this->dataResponse['internal']['errors']['invalid'] = $errors;
    }

    /**
     * @param $e_id
     */
    protected function notFound($e_id)
    {
        $this->response->setStatusCode(Response::HTTP_NOT_FOUND);
        $this->dataResponse['errors'][] = ['not_found' => 'Data not found'];
        $error = [
            'message' => sprintf('Entity with external_id <%s> not found', $e_id),
            'externalId' => $e_id
        ];
        $this->dataResponse['internal']['errors'][] = $error;
        $this->dataResponse['status'] = 0;
    }

    /**
     * @param boolean $active
     * @param LeadingActivityHistory $leadingActivityHistory
     * @param ActiveHistoryInterface $activeHistory
     */
    protected function addActiveHistory($active, LeadingActivityHistory $leadingActivityHistory, ActiveHistoryInterface $activeHistory)
    {
        $activeHistory->setStatus($active);
        $leadingActivityHistory->addActiveHistory($activeHistory);
    }
}
