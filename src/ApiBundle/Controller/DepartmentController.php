<?php

namespace ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DepartmentController
 * @package ApiBundle\Controller
 */
class DepartmentController extends DefaultController
{
    /**
     * DisciplineController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTagName('department');
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function readAction(Request $request)
    {
        $department = $this->get('segment_loader_service')->segment('AppBundle:Department', $request->get('page'))->load();
        if (empty($department)) {
            $this->response->setStatusCode(Response::HTTP_NOT_FOUND);
            $this->dataResponse['errors']['not_found'] = 'Page not found';
            $this->dataResponse['status'] = 0;
        } else {
            $this->dataResponse['data'] = $department;
        }

        return $this->response->setContent(
            $this->renderView('@Api/view.xml.twig', $this->dataResponse)
        );
    }

}
