<?php

namespace ApiBundle\Controller;

use ApiBundle\Form\StudyGroupType;
use AppBundle\Entity\StudyGroup;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class StudyGroupController
 * @package ApiBundle\Controller
 */
class StudyGroupController extends DefaultController
{
    /**
     * DisciplineController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTagName('group');
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function readAction(Request $request)
    {
        $groups = $this->get('segment_loader_service')
            ->segment('AppBundle:StudyGroup', $request->get('page'))
            ->load();
        if (empty($groups)) {
            $this->response->setStatusCode(404);
            $this->dataResponse['errors'][] = 'Page not found';
            $this->dataResponse['status'] = 0;
        } else {
            $this->dataResponse['data'] = $groups;
        }

        return $this->response->setContent(
            $this->renderView('@Api/view.xml.twig', $this->dataResponse)
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();

        $studyGroup = new StudyGroup();
        $form = $this->createForm(StudyGroupType::class, $studyGroup);
        $form->submit($data);
        if (!$form->isValid()) {
            $this->addValidationError((string)$form->getErrors(true, false));

            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }

        try {
            $em->persist($studyGroup);
            $em->flush();
        } catch (\Exception $e) {
            $this->addError($e, $data['externalId']);
        }

        return $this->response = $this->response->setContent(
            $this->renderView('@Api/create.xml.twig', $this->dataResponse)
        );
    }

    /**
     * @param $e_id
     * @param Request $request
     * @return Response
     */
    public function updateAction($e_id, Request $request)
    {
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $studyGroup = $em->getRepository('AppBundle:StudyGroup')->findOneBy(['externalId' => $e_id]);
        if (empty($studyGroup)) {
            $this->notFound($e_id);

            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }
        $form = $this->createForm(StudyGroupType::class, $studyGroup);
        $form->submit($data, false);
        if (!$this->validate($studyGroup)) {
            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }

        try {
            $em->persist($studyGroup);
            $em->flush();
        } catch (\Exception $e) {
            $this->addError($e, $data['externalId']);
        }

        return $this->response = $this->response->setContent(
            $this->renderView('@Api/create.xml.twig', $this->dataResponse)
        );
    }

    /**
     * @param $e_id
     * @return Response
     */
    public function deleteAction($e_id)
    {
        $em = $this->getDoctrine()->getManager();
        $studyGroup = $em
            ->getRepository('AppBundle:StudyGroup')
            ->findOneBy(['externalId' => $e_id]);

        if (empty($studyGroup)) {
            $this->notFound($e_id);

            return $this->response->setContent(
                $this->renderView('@Api/create.xml.twig', $this->dataResponse)
            );
        }

        try {
            $em->remove($studyGroup);
            $em->flush();
        } catch (\Exception $e) {
            $this->addError($e, $e_id);
        }

        return $this->response->setContent(
            $this->renderView('@Api/create.xml.twig', $this->dataResponse)
        );
    }

    /**
     * @param $e_id
     * @return Response
     */
    public function existAction($e_id)
    {
        $this->dataResponse['data'] = '1';
        $em = $this->getDoctrine()->getManager();
        $user = $em
            ->getRepository('AppBundle:StudyGroup')
            ->findOneBy(['externalId' => $e_id]);
        if (empty($user)) {
            $this->dataResponse['data'] = '0';
        }

        return $this->response->setContent(
            $this->renderView('@Api/exist.xml.twig', $this->dataResponse)
        );
    }
}
