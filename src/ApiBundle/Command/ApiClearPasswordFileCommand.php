<?php

namespace ApiBundle\Command;

use ApiBundle\Services\SaveToFileService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\HttpKernel\Kernel;

/**
 * Class ApiClearPasswordFileCommand
 * @package UserBundle\Command
 */
class ApiClearPasswordFileCommand extends ContainerAwareCommand
{
    /**
     * @var SaveToFileService
     */
    private $saveService;

    /**
     * @var Kernel
     */
    private $kernel;

    /**
     * ApiClearPasswordFileCommand constructor.
     * @param string|null $name
     * @param SaveToFileService $saveService
     * @param Kernel $kernel
     */
    public function __construct($name = null, SaveToFileService $saveService, Kernel $kernel)
    {
        parent::__construct($name);
        $this->saveService = $saveService;
        $this->kernel = $kernel;
    }

    protected function configure()
    {
        $this
            ->setName('api:file:passwords:clear')
            ->setDescription('Clear file witch users passwords')
            ->addOption(
                'path',
                'p',
                InputOption::VALUE_REQUIRED,
                'relative path to file. This use only file in dir /app/Resources/files'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getOption('path')) {
            throw new \Exception('Path no valid');
        }

        try {
            $this->saveService->clear(sprintf('%s%s', $this->kernel->getRootDir(), $input->getOption('path')));
            echo 'Done';
        } catch (\Exception $e) {
            echo $e->getMessage();

            echo 1;
        }

        return 0;
    }

}
